#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pygmt
import numpy as np
from pyproj import Proj
import matplotlib.pyplot as plt
import matplotlib as matplotlib
import argparse

parser = argparse.ArgumentParser(description="Plot residuals in map view higher than 90% residuals are plotted in red")
parser.add_argument("-save", help="save figures, default is 0", type=int, default=0)
parser.add_argument("-name", help="path to the output files, default is OUT/", default="OUT/")
parser.add_argument("-utm", help="zone for utm projection", type=int, default=31)
parser.add_argument("-scale", help="scale for plotting vectors", type=float, default = 0.2)

args = parser.parse_args()

path = args.name

# Read the data from the input files
param = open(path+ '/param.out', "r")
lines = param.readlines()
lines = [line.strip() for line in lines]
lines = [line.split() for line in lines]
zone = tuple([float(t)*1000 for t in lines[2]])

coord_data = np.loadtxt(path + "/DataUsed.out")
res1_data = np.loadtxt(path + "/Residuals1.out")
res2_data = np.loadtxt(path + "/Residuals2.out")

myProj = Proj("+proj=utm +zone="+str(args.utm)+" +north +datum=WGS84 +units=m +no_defs")

r0, r2 = myProj(zone[0], zone[2], inverse=True)
r1, r3 = myProj(zone[1], zone[3], inverse=True)
region = [round(r0), round(r1), round(r2), round(r3)]

lon, lat = myProj(coord_data[:, 0] * 1000, coord_data[:, 1] * 1000, inverse=True)
# Perform the desired operations and store the result in residuals_data
residuals_data = np.column_stack((
    lon,
    lat,
    res1_data[:, 0] - res1_data[:, 1],
    res2_data[:, 0] - res2_data[:, 1],
    np.zeros(len(coord_data)),
    np.zeros(len(coord_data)),
    0.1 * np.ones(len(coord_data)), np.ones(len(coord_data))
))

#compute percentile 0.75 (Q3)
h90 = np.percentile((residuals_data[:,2]**2+residuals_data[:,3]**2)**0.5, 90)
highres = (residuals_data[:,2]**2+residuals_data[:,3]**2)**0.5>h90
nrms = np.sum((residuals_data[:,2]**2+residuals_data[:,3]**2)**0.5)/residuals_data.shape[0]

fig = pygmt.Figure()
# Create the basemap
fig.basemap(region=region, frame=True)
# Add coastlines and background
fig.coast(shorelines=True, water="skyblue", resolution="l")

scl=str(args.scale)
fig.velo(data=residuals_data[np.invert(highres)], pen="0.2p,black",line=True,spec="e40",vector="0.1c+e+gblack", scale=scl)
fig.velo(data=residuals_data[highres], pen="0.2p,red",line=True,spec="e40",vector="0.1c+e+gred", scale=scl)

vscale = np.array([[round(r0)+3, round(r3)-2, 4, 0, 0.1, 0.1, 0.1, 0.4]])
fig.velo(data=vscale, pen="1.4p,black",line=True,spec="e40",vector="0.1c+e+gblack", scale=scl)
fig.text(text="4 mm/yr", x=round(r0)+3, y=round(r3)-1.5, font="9p,Helvetica-Bold,black", fill="white")

fig.text(text="nrms : "+ str(float(f'{nrms:.2f}'))+"mm/yr", x=round(r0)+5, y=round(r2)+1, font="9p,Helvetica-Bold,black", fill="white")

if args.save :
    # Save the plot
    # Save the corrected residuals data to a file
    np.savetxt("residuals.vel", residuals_data, fmt="%f %f %f %f %f %f %f %f")
    out = "map.pdf"
    fig.savefig(out)

# Display the plot
fig.show()
