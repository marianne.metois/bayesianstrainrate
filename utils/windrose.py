#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Get the full PDF for principal strain directions at a given point
warning : coordinates given in projected units, take the closest point
run windrose.py -name OUT -x 100 -y 5000
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as matplotlib
import argparse
#import weightedstats

parser = argparse.ArgumentParser(
    description='Get the full PDF for principal strain directions at a given point. warning : coordinates given in projected units, take the closest point. run windrose.py -name OUT -x 100 -y 5000')
parser.add_argument('-save', help='save figures, default is 0', type=int, default=0)
parser.add_argument('-name', help='path to the output files, default is OUT/', default='OUT/')
parser.add_argument('-x', help='x coordinate of the pixel', type=float)
parser.add_argument('-y', help='y coordinate of the pixel', type=float)

args = parser.parse_args()

path = args.name

postlines=np.loadtxt(path + '/Posteriorphi.out') # proba azimtuh first eigen vector
post1lines=np.loadtxt(path + '/Averagelambda1.out') # first eigen value(norm1)
post2lines=np.loadtxt(path + '/Averagelambda2.out') # second eigen value (norm2)

x = args.x
y = args.y

# read the param.out file
param = open(path+ '/param.out', "r")
lines = param.readlines()
lines = [line.strip() for line in lines]
lines = [line.split() for line in lines]
nbrPDF = tuple([int(lines[1][1]), int(lines[1][0]), int(lines[1][2])])
zone = tuple([float(t) for t in lines[2]])

#%%

def simplePloter(radii, amplitude, N):
    '''
         Not applicable package, simple drawing
    '''
    
    theta=np.linspace(np.radians(-40),np.radians(360-40),N*4,endpoint=False)    
    width=np.full(N*4,np.radians(10))    
    colors=plt.cm.seismic(np.append(amplitude,0.5))
    fig = plt.figure()
    ax=plt.subplot(111, projection='polar')
    ax.bar(theta,radii,width=width,color=colors,alpha=1,bottom=0.0)
    fig.colorbar(plt.cm.ScalarMappable(norm=matplotlib.colors.Normalize(-1,1), cmap='seismic'), ax=ax)
    plt.show()

#%%

#nbr of bins for angle description (9 bins = 10° wide bins)
nbpointsangle=9
anglePDF=list(nbrPDF)
anglePDF[2]=nbpointsangle
anglePDF=tuple(anglePDF)

nbit=0
for l in range(nbpointsangle):
    nbit+=postlines[l,2]

l = np.argmin(((postlines[:,0]-x)**2+(postlines[:,1]-y)**2)**0.5)

amplitude=np.zeros(36)
radii=np.zeros(36)
amplitude[0:9],amplitude[9:18],amplitude[18:27],amplitude[27:36] = postlines[l:l+9,2],postlines[l:l+9,2],postlines[l:l+9,2],postlines[l:l+9,2]
radii[0:9],radii[9:18],radii[18:27],radii[27:36] = post1lines[l:l+9,2],post2lines[l:l+9,2],post1lines[l:l+9,2],post2lines[l:l+9,2]

# rescale nstrain
radii=radii*10**9

#put the colors between 0 and 1, turn radii positive
amplitudebis=np.zeros(amplitude.shape)
for i in range(np.size(radii)):
    if radii[i] < 0:
        radii[i]=np.abs(radii[i])
        amplitudebis[i]=0.5+amplitude[i]/(2*nbit)
        amplitude[i]=0.5-amplitude[i]/(2*nbit)
    else :
        amplitudebis[i]=0.5-amplitude[i]/(2*nbit)
        amplitude[i]=0.5+amplitude[i]/(2*nbit)
        
#TURN ON to manually set the ylim since Python seems totally bugged on it
#ylim = 40000000
#for i in range(np.size(radii)):
#    if radii[i] > ylim:
#        radii[i] = ylim

#%%
#PLOT
#simplePloter(radii,amplitude,nbpointsangle)

#figure 1 is full PDF for the chosen point
        
theta=np.linspace(np.radians(-40),np.radians(360-40),nbpointsangle*4,endpoint=False)    
width=np.full(nbpointsangle*4,np.radians(10))    
colors=plt.cm.seismic(np.append(amplitudebis,0.5))
# red stands here for compression, blue for extension


fig, axes1 = plt.subplots(1,1)
axes1=plt.subplot(projection='polar')
#plot full distrib
axes1.bar(theta,radii,width=width,color=colors,alpha=1,bottom=0.0)
#plot max mode principal directions
#can be very different from median/average

#test plot max mode consistent
axes1.bar(theta[np.argmax(amplitude-0.5)], radii[np.argmax(amplitude-0.5)], width=0.05, color='black')
axes1.bar(theta[np.argmax(amplitude-0.5)]+np.pi, radii[np.argmax(amplitude-0.5)], width=0.05, color='black')
axes1.bar(theta[np.argmin(amplitude-0.5)], radii[np.argmin(amplitude-0.5)], width=0.05, color='black')
axes1.bar(theta[np.argmin(amplitude-0.5)]+np.pi, radii[np.argmin(amplitude-0.5)], width=0.05, color='black')

#test plot median unefficient with bimodal distribution and non perpendicular axis
# medanglecomp=weightedstats.numpy_weighted_median(theta[amplitude>0.5][:10], amplitude[amplitude>0.5][:10])
# medangledil=weightedstats.numpy_weighted_median(theta[amplitude<=0.5][:10], amplitude[amplitude<=0.5][:10])

# axes1.bar(medanglecomp, radii[theta==medanglecomp], width=0.05, color='green')
# axes1.bar(medanglecomp+np.pi, radii[theta==medanglecomp], width=0.05, color='green')
# axes1.bar(medangledil, radii[theta==medangledil], width=0.05, color='green')
# axes1.bar(medangledil+np.pi, radii[theta==medangledil], width=0.05, color='green')

#%%
fig.colorbar(plt.cm.ScalarMappable(norm=matplotlib.colors.Normalize(-1,1), cmap='seismic'), ax=axes1)

#figure 2 is the average second invariant map + data points + location of the chosen point
average = np.loadtxt(path + "/Average.out",  comments='#', usecols=(0,1,2,3,4,5,6))
mycmap = plt.get_cmap('bwr')
data = np.loadtxt(path + "/DataUsed.out",  comments='#', usecols=(0,1,2,3))

fig2, axes2 = plt.subplots(1,1)
#axes2=plt.subplot(212, projection='rectilinear')
im1 = axes2.scatter(average[:,0], average[:,1], c=average[:,6], cmap=mycmap, s=3)
axes2.scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
axes2.plot(x,y, color='black', marker='+', markersize=12)
axes2.set_xlim(zone[0:2])
axes2.set_ylim(zone[2:])
fig2.colorbar(im1, ax=axes2, label='Average 2nd Inv')

plt.show()
