#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 17:06:07 2022

@author: marianne

Plot median/maxmode/average for one output
install of weightedstats is needed
can be long if grid is big
"""
import matplotlib.pyplot as plt
import numpy as np
import argparse
import weightedstats


parser = argparse.ArgumentParser(
    description='Plot median/maxmode/average for one output')
parser.add_argument('-save', help='save figures, default is 0', type=int, default=0)
parser.add_argument('-name', help='path to the output files, default is OUT/', default='OUT/')
parser.add_argument('-type', help='value plotted (div, rot, inv, 1, 2)', default='inv')
parser.add_argument('-gtiff', help='save figures as gtiff, default is False', default=False)
parser.add_argument('-maxval', help='impose maximum value for color bar in nstrain/yr', type=float)

args = parser.parse_args()

path = args.name

# read the param.out file
param = open(path+ '/param.out', "r")
lines = param.readlines()
lines = [line.strip() for line in lines]
lines = [line.split() for line in lines]

nbrPDF = tuple([int(lines[1][1]), int(lines[1][0]), int(lines[1][2])])
zone = tuple([float(t) for t in lines[2]])

if args.type == 'div':
    outbounds = tuple([float(t) for t in lines[4]])
    symm = True
elif args.type == 'rot':
    outbounds = tuple([float(t) for t in lines[5]])
    symm = True
elif args.type == 'inv':
    outbounds = tuple([float(t) for t in lines[6]])
    symm = False
elif args.type == '1':
    Vbounds = tuple([float(t) for t in lines[3]])
    outbounds = (Vbounds[0]-Vbounds[1],Vbounds[0]+Vbounds[1])
    symm = False
elif args.type == '2':
    Vbounds = tuple([float(t) for t in lines[3]])
    outbounds = (Vbounds[2]-Vbounds[3],Vbounds[2]+Vbounds[3])
    symm = False

outlong = np.loadtxt(path+ '/Posterior'+args.type +'.out', usecols = 2)
data = np.loadtxt(path + "/DataUsed.out",  comments='#', usecols=(0,1,2,3))
# swapaxes pour se remettre en geographique
# this step takes long
cubeout = np.swapaxes(np.reshape(outlong, nbrPDF), 0, 1)

# create the vector to sample the PDF
xcoord = np.linspace(zone[0], zone[1], num=nbrPDF[0])
ycoord = np.linspace(zone[2], zone[3], num=nbrPDF[1])
lons, lats = np.meshgrid(xcoord,ycoord)
print(xcoord.shape, ycoord.shape)
# create the vector to sample the PDF
outval = np.linspace(outbounds[0], outbounds[1], num=nbrPDF[2])

# for median/average plotting
# necessary to ignore values on the bounds that could be artificially high
outmed = np.zeros(cubeout.shape[0:2])
outmoy = np.zeros(cubeout.shape[0:2])

for i in range(cubeout.shape[0]):
  for j in range(cubeout.shape[1]):
#      print(i,j)
      outmed[i,j]= weightedstats.numpy_weighted_median(outval[1:-1], cubeout[i,j,1:-1])
      outmoy[i,j]= weightedstats.numpy_weighted_mean(outval[1:-1], cubeout[i,j,1:-1])

# for maxmode plotting
# necessary to ignore values on the bounds that could be artificially high
outmax = np.argmax(cubeout[:,:,1:-1], axis=2)+1

#%%
fig, axes = plt.subplots(3,1)
#zone = (110, 145, 5020, 5050)

mycmap = plt.get_cmap('bwr')
if symm :
    if args.maxval : 
        high = args.maxval*10**(-9)
        low = -high
    else : 
        low = np.min(outmed)
        high = -low
else :
    low = 0
    if args.maxval :
        high = args.maxval*10**(-9)
    else :
        high = np.max(outmed)

im = axes[0].pcolormesh(lons,lats,outmoy, cmap=mycmap, shading='nearest', vmin=low, vmax=high)
axes[0].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
axes[0].set_xlim(zone[0:2])
axes[0].set_ylim(zone[2:])
fig.colorbar(im, ax=axes[0], label='Average '+args.type)

im2 = axes[1].pcolormesh(lons,lats,outmed, cmap=mycmap, shading='nearest', vmin=low, vmax=high)
axes[1].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
axes[1].set_xlim(zone[0:2])
axes[1].set_ylim(zone[2:])
fig.colorbar(im2, ax=axes[1], label='Median '+args.type)

# vmin=-0.0001, vmax=0.0002
im3 = axes[2].pcolormesh(lons,lats,outval[outmax], cmap=mycmap, shading='nearest', vmin=low, vmax=high)
axes[2].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
axes[2].set_xlim(zone[0:2])
axes[2].set_ylim(zone[2:])
fig.colorbar(im3, ax=axes[2], label='Max mode '+args.type)


fig.tight_layout()
#%%

if args.save:
    plt.savefig(args.name + '/compar_' + args.type + '.png', bbox_inches='tight')


plt.show()
# xmed=xarray.DataArray(outmed,coords=dict(lon=(["x", "y"], lons),lat=(["x", "y"], lats)),dims=["x","y","median"])

if args.gtiff :
    import pandas as pd
    import rioxarray as rio
    import xarray
    df=pd.DataFrame(np.vstack([outmed.flatten(), lons.flatten()*1000, lats.flatten()*1000]).T, columns=['med','longitude', 'latitude'])
    da=df.set_index(['latitude','longitude']).to_xarray()
    da.med.rio.to_raster(args.name + '/' + args.type+'_med.tiff')
    df=pd.DataFrame(np.vstack([outmoy.flatten(), lons.flatten()*1000, lats.flatten()*1000]).T, columns=['moy','longitude', 'latitude'])
    da=df.set_index(['latitude','longitude']).to_xarray()
    da.moy.rio.to_raster(args.name + '/' + args.type+'_moy.tiff')
    df=pd.DataFrame(np.vstack([outval[outmax].flatten(), lons.flatten()*1000, lats.flatten()*1000]).T, columns=['maxmode','longitude', 'latitude'])
    da=df.set_index(['latitude','longitude']).to_xarray()
    da.maxmode.rio.to_raster(args.name + '/' + args.type+'_maxmode.tiff')
