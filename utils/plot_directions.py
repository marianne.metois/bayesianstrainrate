#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot principal strain/strain rate directions in map view for a selection of points
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as matplotlib
import argparse

parser = argparse.ArgumentParser(description="Plot principal strain direction in map view")
parser.add_argument("-save", help="save figures, default is 0", type=int, default=0)
parser.add_argument("-name", help="path to the output files, default is OUT/", default="OUT/")
parser.add_argument("-step", help="nbr of points for decimating grid, set to 0 if original grid is needed", type=int, default=30)
parser.add_argument("-opt", help="plotting option gmt or quiver", default='gmt')
parser.add_argument("-scale", help="scale for plotting principal direction", type=float)
parser.add_argument("-gtiff", help='save figures as gtiff, default is False', default=False)

args = parser.parse_args()

option = args.opt
path = args.name

data = np.loadtxt(path + "/DataUsed.out", comments="#", usecols=(0, 1, 2, 3))
postlines = np.loadtxt(path + "/Posteriorphi.out")  # proba azimtuh first eigen vector
post1lines = np.loadtxt(path + "/Averagelambda1.out")  # first eigen value(norm1)
post2lines = np.loadtxt(path + "/Averagelambda2.out")  # second eigen value (norm2)

# read the param.out file
param = open(path + "/param.out", "r")
lines = param.readlines()
lines = [line.strip() for line in lines]
lines = [line.split() for line in lines]
nbrPDF = tuple([int(lines[1][1]), int(lines[1][0]), int(lines[1][2])])
zone = tuple([float(t) for t in lines[2]])

# nbr of bins for angle description (9 bins = 10° wide bins)
nbpointsangle = 9
anglePDF = list(nbrPDF)
anglePDF[2] = nbpointsangle
anglePDF = tuple(anglePDF)

#%%
# formattage directions principales
# swapaxes pour se remettre en geographique
# proba max angle
cubedata = np.swapaxes(np.reshape(postlines[:, 2], anglePDF), 0, 1)
xdata = np.swapaxes(np.reshape(postlines[:, 0], anglePDF), 0, 1)
ydata = np.swapaxes(np.reshape(postlines[:, 1], anglePDF), 0, 1)

# create the vector to sample the PDF
valmax = np.argmax(cubedata[:, :, :], axis=2)

l1data = np.swapaxes(np.reshape(post1lines[:, 2], anglePDF), 0, 1)
l2data = np.swapaxes(np.reshape(post2lines[:, 2], anglePDF), 0, 1)

l1M = np.zeros((list(anglePDF)[1], list(anglePDF)[0]))
l2M = np.zeros((list(anglePDF)[1], list(anglePDF)[0]))
angle = np.zeros((list(anglePDF)[1], list(anglePDF)[0]))

for i in range(0, list(anglePDF)[1]):
    for j in range(0, list(anglePDF)[0]):
        l1M[i, j] = l1data[i, j, valmax[i, j]]
        l2M[i, j] = l2data[i, j, valmax[i, j]]
        if l1M[i, j] < l2M[i, j]:
            angle[i, j] = 130 - 10 * valmax[i, j]
            l2M[i, j] = l1data[i, j, valmax[i, j]] # switch pour format velo
            l1M[i, j] = l2data[i, j, valmax[i, j]] # switch pour format velo
        else:
            angle[i, j] = 220 - 10 * valmax[i, j]

# convert cartesian coordinates to row,column numbers
xcoord = np.linspace(zone[0], zone[1], num=int(nbrPDF[0]))
ycoord = np.linspace(zone[2], zone[3], num=int(nbrPDF[1]))
lons, lats = np.meshgrid(xcoord, ycoord)

#%%
step = args.step
if step == 0 : 
     x50, y50 = np.meshgrid(np.array(list(range(0,nbrPDF[1],1))),
                       np.array(list(range(0,nbrPDF[0],1))))
else :
     x50, y50 = np.meshgrid(np.array(list(range(0,nbrPDF[1]-1,int(nbrPDF[1]/step)))), 
                       np.array(list(range(0,nbrPDF[0]-1,int(nbrPDF[0]/step)))))
x50 = x50.flatten()
y50 = y50.flatten()

#select = np.array([np.stack((angle[i,j], l1M[i,j], l2M[i,j])) 
#                   for i,j in zip(*(x50.astype(np.int), y50.astype(np.int)))])

select = np.array([np.stack((xdata[i,j,0], ydata[i,j,0], angle[i,j], l1M[i,j]*10**9, l2M[i,j]*10**9)) 
                   for i,j in zip(*(x50.astype(int), y50.astype(int)))])
select2 = np.stack((select[:,0], select[:,1], select[:,3], select[:,4], select[:,2])).T

if (option == 'gmt'):
    import pygmt
#%%
#test plotting with pygmt
#**1**,\ **2**: longitude, latitude of station
#**3**: eps1, the most extensional eigenvalue of strain tensor,with extension taken positive.
#**4**: eps2, the most compressional eigenvalue of strain tensor,with extension taken positive.
#**5**: azimuth of eps2 in degrees CW from North.
    scl=str(0.2)
    if args.scale:
        scl=str(args.scale)
    fig = pygmt.Figure()
    fig.basemap(region=np.array(zone), frame=["WSne", "40f"], projection="x0.2/0.1",) 
    #fig.coast(projection="U34T/2", land="lightgreen",    water="lightblue",    shorelines="thinnest",    frame="afg")
    fig.velo(data=select2, pen="0.6p,black",line=True,spec="x"+ scl,vector="4.0c+p1p+e+gblack",)
    fig.show()
    
    if args.gtiff:
        import pandas as pd
        import rioxarray as rio
        import xarray
        df=pd.DataFrame(np.vstack([(select[:,2], select[:,0]*1000, select[:,1]*1000)]).T, columns=['eps1','longitude', 'latitude'])
        da=df.set_index(['latitude','longitude']).to_xarray()
        da.eps1.rio.to_raster(args.name + '/eps1.tiff')
        df=pd.DataFrame(np.vstack([(select[:,3], select[:,0]*1000, select[:,1]*1000)]).T, columns=['eps2','longitude', 'latitude'])
        da=df.set_index(['latitude','longitude']).to_xarray()
        da.eps2.rio.to_raster(args.name + '/eps2.tiff')
        df=pd.DataFrame(np.vstack([(select[:,4], select[:,0]*1000, select[:,1]*1000)]).T, columns=['az','longitude', 'latitude'])
        da=df.set_index(['latitude','longitude']).to_xarray()
        da.az.rio.to_raster(args.name + '/az.tiff')
   
else:
   Ux1 = np.array([np.sin(np.radians(select[i,2]))*
                select[i, 4] 
                for i in list(range(0,select.shape[0]))])
   Uy1 = np.array([np.cos(np.radians(select[i,2]))*select[i, 4]
                for i in list(range(0,select.shape[0]))])

   sign1 = np.array([select[i, 4]<0 
                  for i in list(range(0,select.shape[0]))])
    
   Ux2 = np.array([np.sin(np.radians(select[i,2])+np.pi/2)*
                select[i, 3]
                for i in list(range(0,select.shape[0]))])
   Uy2 = np.array([np.cos(np.radians(select[i,2])+np.pi/2)*
                select[i, 3] 
                for i in list(range(0,select.shape[0]))])

   sign2 = np.array([select[i, 3]<0 
                  for i in list(range(0,select.shape[0]))])

# extension taken positive would stand blue, compression would stand red
   mycmap = plt.get_cmap('bwr')
   fig1, axe1 = plt.subplots(1,1)
   scl=10
   if args.scale:
       scl=args.scale
   axe1.quiver(select[:,0], select[:,1], Ux1*scl, Uy1*scl, sign1*1, pivot='mid',
           headwidth=0, headlength=0, headaxislength=0, cmap=mycmap, 
           norm= matplotlib.colors.Normalize(vmin=0, vmax=1), angles='uv', scale=10**5, scale_units='inches')
           
   axe1.quiver(select[:,0], select[:,1], Ux2*scl, Uy2*scl, sign2*1, pivot='mid', 
           headwidth=0, headlength=0, headaxislength=0, cmap=mycmap, 
           norm= matplotlib.colors.Normalize(vmin=0, vmax=1), angles='uv', scale=10**5, scale_units='inches')
           
   axe1.scatter(data[:,0], data[:,1], s=0.5, marker='.', color='black' )
   axe1.set_xlim(zone[0:2])
   axe1.set_ylim(zone[2:])
   plt.show()
