#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 17:06:07 2022

@author: marianne

Check results from Bayesian Strain Rates inversion
"""
import matplotlib.pyplot as plt
import numpy as np
import argparse

parser = argparse.ArgumentParser(
    description='check if inversion is ok with simple graphs and average maps')
parser.add_argument('-save', help='save figures, default is 0', type=int, default=0)
parser.add_argument('-name', help='path to the output files, default is OUT/', default='OUT/')
parser.add_argument('-gtiff', help='save std as gtiff, default is False', default=False)

args = parser.parse_args()


path = args.name

print('***** check if convergence is reached ******')
cells = np.loadtxt(path + "NB_cells.out",  comments='#', usecols=(0,1))
sigm = np.loadtxt(path + "conv_sigma.out",  comments='#', usecols=(0))
evid = np.loadtxt(path + "Evidence.out",  comments='#', usecols=(0))
data = np.loadtxt(path + "DataUsed.out",  comments='#', usecols=(0,1,2,3))


fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
ax1.plot(cells[:,0])
ax1.set_ylabel('Nbr of triangles')
#ax1.set_xlabel('Number of iterations')

ax2.plot(sigm)
ax2.set_ylabel('scaling factor')
ax2.set_xlabel('Number of iterations')

ax3.bar(np.array(range(0,evid.size)), evid, width=1, color='blue')
ax3.set_xlabel('Number of triangles')
ax3.set_ylabel('models kept')

ar1v = 'None'
ar1p = 'None'
arBD = 'None'
with open( path+ "mpi.out", "r") as file: 
     for line in file: 
         if "AR1v" in line: 
             line = line.rstrip()
             ar1v = line
         if "AR1p"  in line: 
             line = line.rstrip()
             ar1p = line
         if "ARB"  in line: 
             line = line.rstrip()
             arBD = line
#             arBD = line.split() 

print('****** AR rates should be close to 44\% to ensure good posterior sampling ******')
print(ar1v)
print(ar1p)
print('****** AR rates for death and birth of nodes should be as high as possible ******')
print(arBD)

fig.tight_layout()
if args.save:
    plt.savefig(args.name + '/graphs.png', bbox_inches='tight')

#plt.show()

print('**** plot average maps *****')
print('careful, averages can be misleading, check the entire PDF distribution')
average = np.loadtxt(path + "Average.out",  comments='#', usecols=(0,1,2,3,4,5,6))


## (X, Y, average Vx, average Vy, average divergence, average vorticity, average 2nd inv)
fig2, axs = plt.subplots(3, 2)
mycmap = plt.get_cmap('bwr')
axs[-1, -1].axis('off')
im0 = axs[0,0].scatter(average[:,0], average[:,1], c=average[:,2], cmap=mycmap)
fig2.colorbar(im0, ax=axs[0,0], label='Vx')
axs[0,0].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
im1 = axs[1,0].scatter(average[:,0], average[:,1], c=average[:,3], cmap=mycmap)
fig2.colorbar(im1, ax=axs[1,0], label='Vy')
axs[1,0].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
im2 = axs[2,0].scatter(average[:,0], average[:,1], c=average[:,4], cmap=mycmap)
fig2.colorbar(im2, ax=axs[2,0], label='div')
axs[2,0].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
im3 = axs[0,1].scatter(average[:,0], average[:,1], c=average[:,5], cmap=mycmap)
fig2.colorbar(im3, ax=axs[0,1], label='rot')
axs[0,1].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
im4 = axs[1,1].scatter(average[:,0], average[:,1], c=average[:,6], cmap=mycmap)
fig2.colorbar(im4, ax=axs[1,1], label='2ndinv')
axs[1,1].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')

fig2.tight_layout()
if args.save:
    plt.savefig(args.name + '/average_maps.png', bbox_inches='tight')

density = np.loadtxt(path + "Density.out",  comments='#', usecols=(0,1,2), skiprows=4)
fig3, axs = plt.subplots(1,1)
mycmap = plt.get_cmap('YlOrRd')

im = axs.scatter(density[:,0], density[:,1], c=density[:,2], cmap=mycmap)
fig3.colorbar(im, ax=axs, label='Node density')
axs.scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')

if args.save:
    plt.savefig(args.name + '/node_density.png', bbox_inches='tight')

print('Map of standard deviation on velocity component can help building a confidence mask')

std1 = np.loadtxt(path + "Standard_deviation1.out",  comments='#', usecols=(0,1,2), skiprows=4)
std2 = np.loadtxt(path + "Standard_deviation2.out",  comments='#', usecols=(0,1,2), skiprows=4)

if args.gtiff :
    print('saving average horizontal STD as geotiff')
    import pandas as pd
    import rioxarray as rio
    import xarray
    hstd = (std1[:,2]**2+std2[:,2]**2)**0.5
    df=pd.DataFrame(np.vstack([hstd.flatten(), std1[:,0].flatten()*1000, std1[:,1].flatten()*1000]).T, columns=['hstd','longitude', 'latitude']).drop_duplicates()
    da=df.set_index(['latitude','longitude']).to_xarray()
    da.hstd.rio.to_raster(args.name + '/' + 'hstd.tiff')
    print('saving node density as geotiff')
    df=pd.DataFrame(np.vstack([density[:,2].flatten(), density[:,0].flatten()*1000, density[:,1].flatten()*1000]).T, columns=['density','longitude', 'latitude']).drop_duplicates()
    da=df.set_index(['latitude','longitude']).to_xarray()
    da.density.rio.to_raster(args.name + '/' + 'density.tiff')
    
fig4, axs = plt.subplots(1,2)
mycmap = plt.get_cmap('YlOrRd')
im = axs[0].scatter(std1[:,0], std1[:,1], c=std1[:,2], cmap=mycmap)
fig4.colorbar(im, ax=axs[0], label='Std V1')
axs[0].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
im2 = axs[1].scatter(std2[:,0], std2[:,1], c=std2[:,2], cmap=mycmap)
fig4.colorbar(im2, ax=axs[1], label='Std V2')
axs[1].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')

fig4.tight_layout()

if args.save:
    plt.savefig(args.name + '/Std V.png', bbox_inches='tight')

    
plt.show()
