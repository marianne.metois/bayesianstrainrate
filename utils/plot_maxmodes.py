#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 17:06:07 2022

@author: marianne

Plot max mode for all outputs
"""
import matplotlib.pyplot as plt
import numpy as np
import argparse

parser = argparse.ArgumentParser(
    description='extract max mode')
parser.add_argument('-save', help='save figures, default is 0', type=int, default=0)
parser.add_argument('-name', help='path to the output files, default is OUT/', default='OUT/')

args = parser.parse_args()

path = args.name

# read the param.out file
param = open(path+ '/param.out', "r")
lines = param.readlines()
lines = [line.strip() for line in lines]
lines = [line.split() for line in lines]

nbrPDF = tuple([int(lines[1][1]), int(lines[1][0]), int(lines[1][2])])
zone = tuple([float(t) for t in lines[2]])
divbounds = tuple([float(t) for t in lines[4]])
rotbounds = tuple([float(t) for t in lines[5]])
invbounds = tuple([float(t) for t in lines[6]])
Vbounds = tuple([float(t) for t in lines[3]])


#nbrPDF = (200,200,200)
#zone = (36,212,4949,5115)

divlong = np.loadtxt(path+ '/Posteriordiv.out', usecols = 2)
VXlong = np.loadtxt(path+ '/Posterior1.out', usecols = 2)
VYlong = np.loadtxt(path+ '/Posterior2.out', usecols = 2)
invlong = np.loadtxt(path+ '/Posteriorinv.out', usecols = 2)
rotlong = np.loadtxt(path+ '/Posteriorrot.out', usecols = 2)
data = np.loadtxt(path + "/DataUsed.out",  comments='#', usecols=(0,1,2,3))


# swapaxes pour se remettre en geographique

cubediv = np.swapaxes(np.reshape(divlong, nbrPDF), 0, 1)
cubeVX = np.swapaxes(np.reshape(VXlong, nbrPDF), 0, 1)
cubeVY = np.swapaxes(np.reshape(VYlong, nbrPDF), 0, 1)
cubeinv = np.swapaxes(np.reshape(invlong, nbrPDF), 0, 1)
cuberot = np.swapaxes(np.reshape(rotlong, nbrPDF), 0, 1)

# pb: need to save the parameters.in to get bounds
# div -0.03 0.03
# create the vector to sample the PDF
xcoord = np.linspace(zone[0], zone[1], num=nbrPDF[0])
ycoord = np.linspace(zone[2], zone[3], num=nbrPDF[1])
lons, lats = np.meshgrid(xcoord,ycoord)
# create the vector to sample the PDF
divval = np.linspace(divbounds[0], divbounds[1], num=nbrPDF[2])
VXval = np.linspace(Vbounds[0]-Vbounds[1],Vbounds[0]+Vbounds[1], num=nbrPDF[2])
VYval = np.linspace(Vbounds[2]-Vbounds[3],Vbounds[2]+Vbounds[3], num=nbrPDF[2])
invval = np.linspace(invbounds[0],invbounds[1], num=nbrPDF[2])
rotval = np.linspace(rotbounds[0],rotbounds[1], num=nbrPDF[2])

# for maxmode plotting
# necessary to ignore values on the bounds that could be artificially high

divmax = np.argmax(cubediv[:,:,1:-1], axis=2)+1
VXmax = np.argmax(cubeVX[:,:,1:-1], axis=2)+1
VYmax = np.argmax(cubeVY[:,:,1:-1], axis=2)+1
invmax = np.argmax(cubeinv[:,:,1:-1], axis=2)+1
rotmax = np.argmax(cuberot[:,:,1:-1], axis=2)+1

fig, axes = plt.subplots(3,2)
axes[-1, -1].axis('off')

##im = plt.imshow(divval[divmax])
##fig.colorbar(im, ax=ax0, label='Max mod div')
#im = plt.imshow(VXval[VXmax], origin='lower', extent=[36,212,4949,5115])
#plt.imshow(cubediv[:,:,100])
mycmap = plt.get_cmap('bwr')
im = axes[0,0].pcolormesh(lons,lats,VXval[VXmax], cmap=mycmap, shading='nearest')
axes[0,0].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
fig.colorbar(im, ax=axes[0,0], label='Max mod VX')

im2 = axes[1,0].pcolormesh(lons,lats,VYval[VYmax], cmap=mycmap, shading='nearest')
axes[1,0].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
fig.colorbar(im2, ax=axes[1,0], label='Max mod VY')

# vmin=-0.0001, vmax=0.0002
im3 = axes[2,0].pcolormesh(lons,lats,divval[divmax], cmap=mycmap, shading='nearest')
axes[2,0].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
fig.colorbar(im3, ax=axes[2,0], label='Max mod Div')

im4 = axes[0,1].pcolormesh(lons,lats,rotval[rotmax], cmap=mycmap, shading='nearest')
axes[0,1].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
fig.colorbar(im4, ax=axes[0,1], label='Max mod Rot')

im5 = axes[1,1].pcolormesh(lons,lats,invval[invmax], cmap=mycmap, shading='nearest')
axes[1,1].scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
fig.colorbar(im5, ax=axes[1,1], label='Max mod Inv')


fig.tight_layout()

if args.save:
    plt.savefig(args.name + '/maxmodes.png', bbox_inches='tight')


plt.show()
