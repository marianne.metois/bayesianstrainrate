#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 17:06:07 2022

@author: marianne

Plot cross sections in PDF cubes 
install of weightedstats is needed
"""
import matplotlib.pyplot as plt
import numpy as np
import argparse
import weightedstats
import pandas as pd

parser = argparse.ArgumentParser(
    description='extract profile')
parser.add_argument('-save', help='save figures, default is 0', type=int, default=0)
parser.add_argument('-name', help='path to the output files, default is OUT/', default='OUT/')
parser.add_argument('-A', help='(x,y) coordinates of the first edge of the profile', type=int,  nargs=2)
parser.add_argument('-B', help='(x,y) coordinates of the second edge of the profile', type=int,  nargs=2)
parser.add_argument('-type', help='value plotted (div, rot, inv, VX, VY)', default='inv')
parser.add_argument('-maxval', help='give maxval for plotting in 10-9/yr', type = float)

args = parser.parse_args()


path = args.name
value = args.type

# read the param.out file
param = open(path+ '/param.out', "r")
lines = param.readlines()
lines = [line.strip() for line in lines]
lines = [line.split() for line in lines]

nbrPDF = tuple([int(lines[1][1]), int(lines[1][0]), int(lines[1][2])])
zone = tuple([float(t) for t in lines[2]])
bounds = tuple([float(t) for t in lines[6]])
if value == 'div':
    bounds = tuple([float(t) for t in lines[4]])
elif value == 'rot':
    bounds = tuple([float(t) for t in lines[5]])
elif value == 'VX':
    Vbounds = tuple([float(t) for t in lines[3]])
    bounds = (Vbounds[0]-Vbounds[1],Vbounds[0]+Vbounds[1])
elif value == 'VY':
    Vbounds = tuple([float(t) for t in lines[3]])
    bounds = (Vbounds[2]-Vbounds[3],Vbounds[2]+Vbounds[3])
    
# define cross section
AXY = args.A
BXY = args.B
if all(x > nbrPDF[0] for x in (AXY[0],BXY[0])):
    print('bad X coordinate value')
    exit()
if all(y > nbrPDF[1] for y in (AXY[1],BXY[1])):
    print('bad Y coordinate value')
    exit()

xcoord = np.linspace(zone[0], zone[1], num=nbrPDF[0])
ycoord = np.linspace(zone[2], zone[3], num=nbrPDF[1])
lon, lat = np.meshgrid(xcoord,ycoord)
#bug
print('plotting profile between', lon[AXY[1],AXY[0]], lat[AXY[1],AXY[0]], 'and', lon[BXY[1],BXY[0]], lat[BXY[1],BXY[0]],)

#define cross-section coordinates
alpha = (BXY[1]-AXY[1])/(BXY[0]-AXY[0])
#xcross = np.arange(AXY[0],BXY[0],1)
#ycross = np.arange(AXY[1],BXY[1],1)
distance = np.sqrt((lon[AXY[1]][AXY[0]]-lon[BXY[1]][BXY[0]])**2+(lat[AXY[1]][AXY[0]]-lat[BXY[1]][BXY[0]])**2)
num = int(np.hypot(abs(AXY[0]-BXY[0]), abs(AXY[1]-BXY[1])))
point = np.linspace(0,distance,num)
x, y = np.linspace(AXY[0],BXY[0], num), np.linspace(AXY[1], BXY[1], num)

vallong = np.loadtxt(path+ '/Posteriorinv.out', usecols = 2)
if value == 'div':
    vallong = np.loadtxt(path+ '/Posteriordiv.out', usecols = 2)
elif value == 'VX':
    vallong = np.loadtxt(path+ '/Posterior1.out', usecols = 2)
elif value == 'VY': 
    vallong = np.loadtxt(path+ '/Posterior2.out', usecols = 2)
elif value == 'rot':
    vallong = np.loadtxt(path+ '/Posteriorrot.out', usecols = 2)

data = np.loadtxt(path + "/DataUsed.out",  comments='#', usecols=(0,1,2,3))


# # swapaxes pour se remettre en geographique
cubedata = np.swapaxes(np.reshape(vallong, nbrPDF), 0, 1)

# # create the vector to sample the PDF
valval = np.linspace(bounds[0], bounds[1], num=nbrPDF[2])

# for maxmode plotting
# necessary to ignore values on the bounds that could be artificially high

valmax = np.argmax(cubedata[:,:,1:-1], axis=2)+1

pdfval = np.array([cubedata[x,y,1:-1] for x,y in zip(*(y.astype(int), x.astype(int)))])
print('ad hoc normalization of the PDF')
pdfval = pdfval/np.max(pdfval, axis=1)[:,np.newaxis]
#pdfval = pdfval/pdfval.sum(axis=1)[:,np.newaxis]

pval_max = [valval[np.argmax(cubedata[x,y,1:-1])+1]
            for x,y in zip(*(y.astype(int), x.astype(int)))]
pval_med = [weightedstats.numpy_weighted_median(valval[1:-1], cubedata[x,y,1:-1]) 
            for x,y in zip(*(y.astype(int), x.astype(int)))]
pval_moy = [np.average(valval[1:-1], weights=cubedata[x,y,1:-1]) 
            for x,y in zip(*(y.astype(int), x.astype(int)))]


#%%
fig, (axes1, axes2) = plt.subplots(2,1)
#mycmap = plt.get_cmap('jet_r')
mycmap = plt.get_cmap('hot_r')
im= axes1.pcolormesh(point, valval[1:-1], pdfval.T, cmap=mycmap, shading='nearest')
axes1.plot([0,distance],[0,0],color = 'gray', linestyle='dashed')
axes1.plot(point,pval_max, color ='g', label='Max Mode')
axes1.plot(point,pval_med, color ='b', label='Median')
axes1.plot(point,pval_moy, color ='pink', label='Average')
if args.maxval : 
    if args.type == 'div' or args.type == 'rot' :
        axes1.set_ylim(-args.maxval*10**-9, args.maxval*10**-9)
    else : 
        axes1.set_ylim(0, args.maxval*10**-9)
axes1.set_xlabel("Distance (km)")
axes1.set_ylabel(value)
axes1.legend()
fig.colorbar(im, ax=axes1, label='Normalized PDF') 

#im2 = axes2.pcolormesh(lon,lat,valval[valmax], cmap=mycmap, shading='nearest')

valmed = np.zeros(cubedata.shape[0:2])
for i in range(cubedata.shape[0]):
  for j in range(cubedata.shape[1]):
      valmed[i,j]= weightedstats.numpy_weighted_median(valval[1:-1], cubedata[i,j,1:-1])
im2 = axes2.pcolormesh(lon,lat, valmed, cmap=mycmap, shading='nearest')

axes2.scatter(data[:,0], data[:,1], s=0.1, marker='.', color='black')
axes2.scatter([lon[x][y] for x,y in zip(*(y.astype(int), x.astype(int)))],
              [lat[x][y] for x,y in zip(*(y.astype(int), x.astype(int)))],
              marker='s', s=1, color='gray')
axes2.set_xlim(zone[0:2])
axes2.set_ylim(zone[2:])
fig.colorbar(im2, ax=axes2, label='Median_' + value)


fig.tight_layout()
#%%

if args.save:
    plt.savefig(args.name + '/csection_' + value + '_' + str(AXY[0]) + '-' + str(AXY[1])+ str(BXY[0]) +'-' + str(BXY[1]) + '.png', bbox_inches='tight')
    df = pd.DataFrame({'lon':(lon*1000).flat,'lat':(lat*1000).flat,'maxmod':valval[valmax].flat})
    df.to_csv(args.name + '/modmax_' + args.type + '.txt', sep=' ', header=False, float_format='%.12f', index=False)

plt.show()
