#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 17:06:07 2022

@author: marianne

Plot PDFs for all outputs at one given pixel
Run it properly once in spyder to read the cubes
then change x and y definition to get PDF for pixels quickly and rerun plot cell.
install of weightedstats is needed
"""
import matplotlib.pyplot as plt
import numpy as np
import argparse
import weightedstats

parser = argparse.ArgumentParser(
    description='Plot PDFs for all outputs at one given pixel. Run it properly once in spyder to read the cubes. then change x and y definition to get PDF for pixels quickly and rerun plot cell. install of weightedstats is needed')
parser.add_argument('-save', help='save figures, default is 0', type=int, default=0)
parser.add_argument('-name', help='path to the output files, default is OUT/', default='OUT/')
parser.add_argument('-x', help='x coordinate of the pixel', type=int)
parser.add_argument('-y', help='y coordinate of the pixel', type=int)

args = parser.parse_args()


path = args.name

# read the param.out file
param = open(path+ '/param.out', "r")
lines = param.readlines()
lines = [line.strip() for line in lines]
lines = [line.split() for line in lines]

nbrPDF = tuple([int(lines[1][1]), int(lines[1][0]), int(lines[1][2])])
zone = tuple([float(t) for t in lines[2]])
divbounds = tuple([float(t) for t in lines[4]])
rotbounds = tuple([float(t) for t in lines[5]])
invbounds = tuple([float(t) for t in lines[6]])
Vbounds = tuple([float(t) for t in lines[3]])

x = args.x
y = args.y

divlong = np.loadtxt(path+ '/Posteriordiv.out', usecols = 2)
VXlong = np.loadtxt(path+ '/Posterior1.out', usecols = 2)
VYlong = np.loadtxt(path+ '/Posterior2.out', usecols = 2)
invlong = np.loadtxt(path+ '/Posteriorinv.out', usecols = 2)
rotlong = np.loadtxt(path+ '/Posteriorrot.out', usecols = 2)
data = np.loadtxt(path + "/DataUsed.out",  comments='#', usecols=(0,1,2,3))


# swapaxes pour se remettre en geographique

cubediv = np.swapaxes(np.reshape(divlong, nbrPDF), 0, 1)
cubeVX = np.swapaxes(np.reshape(VXlong, nbrPDF), 0, 1)
cubeVY = np.swapaxes(np.reshape(VYlong, nbrPDF), 0, 1)
cubeinv = np.swapaxes(np.reshape(invlong, nbrPDF), 0, 1)
cuberot = np.swapaxes(np.reshape(rotlong, nbrPDF), 0, 1)

# pb: need to save the parameters.in to get bounds
# div -0.03 0.03
# create the vector to sample the PDF
xcoord = np.linspace(zone[0], zone[1], num=nbrPDF[0])
ycoord = np.linspace(zone[2], zone[3], num=nbrPDF[1])
lon, lat = np.meshgrid(xcoord,ycoord)
print('plotting pixel of coordinates', lon[x][y], lat[y][x])
# create the vector to sample the PDF
divval = np.linspace(divbounds[0], divbounds[1], num=nbrPDF[2])
VXval = np.linspace(Vbounds[0]-Vbounds[1],Vbounds[0]+Vbounds[1], num=nbrPDF[2])
VYval = np.linspace(Vbounds[2]-Vbounds[3],Vbounds[2]+Vbounds[3], num=nbrPDF[2])
invval = np.linspace(invbounds[0],invbounds[1], num=nbrPDF[2])
rotval = np.linspace(rotbounds[0],rotbounds[1], num=nbrPDF[2])

#%%
print('plotting pixel of coordinates', lon[x][y], lat[y][x])
fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
ax1.bar(divval[1:-1], cubediv[x,y,1:-1], width=(np.abs(divbounds[0])+np.abs(divbounds[1]))/nbrPDF[2])
ax1.vlines(divval[np.argmax(cubediv[x,y,1:-1])+1], 0, np.max(cubediv[x,y,1:-1]), colors='purple', label='Max mode')
ax1.vlines(np.average(divval[1:-1], weights=cubediv[x,y,1:-1]), 0, np.max(cubediv[x,y,1:-1]), colors='green', label = 'Average')
ax1.vlines(weightedstats.numpy_weighted_median(divval[1:-1], cubediv[x,y,1:-1]), 0, np.max(cubediv[x,y,1:-1]), colors='orange', label = 'Median')
ax1.set_xlabel('Divergence')
ax1.legend()

ax2.bar(rotval[1:-1], cuberot[x,y,1:-1], width=(np.abs(rotbounds[0])+np.abs(rotbounds[1]))/nbrPDF[2])
ax2.vlines(rotval[np.argmax(cuberot[x,y,1:-1])+1], 0, np.max(cuberot[x,y,1:-1]), colors='purple', label='Max mode')
ax2.vlines(np.average(rotval[1:-1], weights=cuberot[x,y,1:-1]), 0, np.max(cuberot[x,y,1:-1]), colors='green', label = 'Average')
ax2.vlines(weightedstats.numpy_weighted_median(rotval[1:-1], cuberot[x,y,1:-1]), 0, np.max(cuberot[x,y,1:-1]), colors='orange', label = 'Median')
ax2.set_xlabel('Vorticity')
ax2.legend()

ax3.bar(invval[1:-1], cubeinv[x,y,1:-1], width=(np.abs(invbounds[0])+np.abs(invbounds[1]))/nbrPDF[2])
ax3.vlines(invval[np.argmax(cubeinv[x,y,1:-1])+1], 0, np.max(cubeinv[x,y,1:-1]), colors='purple', label='Max mode')
ax3.vlines(np.average(invval[1:-1], weights=cubeinv[x,y,1:-1]), 0, np.max(cubeinv[x,y,1:-1]), colors='green', label = 'Average')
ax3.vlines(weightedstats.numpy_weighted_median(invval[1:-1], cubeinv[x,y,1:-1]), 0, np.max(cubeinv[x,y,1:-1]), colors='orange', label = 'Median')
ax3.set_xlabel('2nd invariant')
ax3.legend()

fig.tight_layout()
#%%

if args.save:
    plt.savefig(args.name + '/pdf_' + str(x) + '_' + str(y) + '.png', bbox_inches='tight')

plt.show()
