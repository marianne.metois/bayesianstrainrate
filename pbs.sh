#!/bin/bash  

###############################################################
#                                                             #
#    Bourne shell script for submitting a parallel MPICH2 job #
#    to the TORQUE queue using the qsub command.              #
#                                                             #
###############################################################

#     Remarks: A line beginning with # is a comment.
#	       A line beginning with #PBS is a PBS directive.
#              PBS directives must come first; any directives
#                 after the first executable statement are ignored.
#
###### PBS -l nodes=1:nodes:ppn=16
###### PBS -l nodes=1:master:ppn=16
###### pour demander tous les noeuds + le master (ppn : processeurs)
###### PBS -l nodes=4:nodes:ppn=16+master:ppn=28
###### PBS -l nodes=1:node1:ppn=4+node2:ppn=13+2:nodes:ppn=16+master:ppn=28
   
#   The PBS directives   

#PBS -N myDeformation 
#PBS -l nodes=2:nodes:ppn=16
##PBS -l walltime=9000:00:00
##PBS -l walltime=01:00:00
##PBS -q nolimit
#PBS -q short
#PBS -o /YOUR_PATH_TO_CHANGE/OUT/out.out
#PBS -j oe
#PBS -r n 
#PBS -l mem=60gb
## PBS -m abe

# Set an ID for MPI daemon  
# THIS PARAMETER IS MANDATORY !
# export MPD_CON_EXT=$PBS_JOBID
#rm /YOUR_PATH_TO_CHANGE/OUT/out.out
echo ------------------------------------------------
echo ' This job is allocated on '${NCPU}' cpu(s)'
echo 'Job is running on node(s): '
echo $PBS_NODEFILE
cat $PBS_NODEFILE

cd YOUR_PATH_TO_CHANGE
export program=YOUR_PATH_TO_CHANGE/run

# Launching mpi program
# echo
# echo Running parallel program:
# echo mpiexec -machinefile $PBS_NODEFILE -np $NCPU $PROG_NAME

# mpiexec -machinefile $PBS_NODEFILE -np $NCPU $PROG_NAME > ~/RJ_MCMC_RF/OUT/mpi.out

#cmd="mpirun -hostfile $PBS_NODEFILE /bin/date  > ~/Cross-convolution/RJ_MCMC_RF/OUT/mpi.out"
#cmd="mpirun -hostfile $PBS_NODEFILE $program > ~/Cross-convolution/RJ_MCMC_RF/OUT/mpi.out"
#echo $cmd
#$cmd

#---------------------------
mpirun --hostfile $PBS_NODEFILE $program > YOUR_PATH_TO_CHANGE/OUT/mpi.out
#-------------------------------------

exit
