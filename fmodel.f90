subroutine fmodel(voro,ncell,location,npoint,dest1,dest2,npointm,ncell_max,nt_max,nmax,nv_max,eps)
  implicit none

  interface
    function solve3(A,C)
    double precision, intent(in) :: A(3,3)   !! Matrix
    double precision, intent(in) :: C(3)   !! Inverse matrix
    double precision             :: detinv,solve3(3),B(3,3)
    end function
  end interface    

  integer ncell_max
  integer nt_max
  integer nmax,nv_max
  double precision  eps 
  
  integer           npointm,p(3)
  integer           vertices(3,nt_max)
  integer           neighbour(3,nt_max)
  integer           nnn(ncell_max+1)
  integer           nnlist(nmax)
  integer           ntwork(nmax)
  integer           walk
  integer           worki1(nv_max)
  integer           worki2(nv_max)
  integer           worki3(nv_max)

  logical	    ldummy(ncell_max),boolval


  double precision Voro(4,ncell_max),location(npointm,2),pos1(3),pos2(3),X(3),val1,val2,A(3,3),C(3) !COLIN
  integer ncell,nt,node,npoint,i,loc,k,iface
  real dest1(npointm),dest2(npointm)!COLIN

! write(*,*)
! write(*,*)
! do i=1,npoint
! write(*,*)location(i,1:2)
! enddo
! write(*,*)
! write(*,*)

!write(*,*)'compute triangulation '

call delaun (Voro(1:2,:),ncell,neighbour,vertices,nt,nt_max,&
                      worki1,worki2,worki3,eps,nv_max,&
                      0,ldummy,0,0,0)
call build_nv(ncell,vertices,nt,ncell_max,nmax,&
              neighbour,nnn,nnlist,ntwork)

!write(*,*)'triangulation computed'

loc=1
do i=1,npoint
        call Triloc_del(location(i,1),location(i,2),Voro(1:2,:),vertices,neighbour,loc,eps,boolval,k,iface)
        p=vertices(1:3,loc)
        pos1=Voro(1,p(1:3))
        pos2=Voro(2,p(1:3))
        C(1)=location(i,1)
        C(2)=location(i,2)
        C(3)=1
        A(1,1:3)=pos1
        A(2,1:3)=pos2
        A(3,1:3)=(/ 1,1,1 /)
        X=solve3(A,C)
        val1=dot_product(X,Voro(3,p(1:3)))
        val2=dot_product(X,Voro(4,p(1:3)))

	dest1(i)=val1
        dest2(i)=val2
! write(*,*)i,node,dest(i)

enddo

return
end

!-------------------------------------------------------------------
!Function needed to calculate the barycenter coefficients associated with a
!position inside a triangle
!------------------------------------------------------------------

function solve3(A,C)
    !! Performs a direct calculation of the solution of a 3×3 system.
    double precision, intent(in) :: A(3,3)   !! Matrix
    double precision, intent(in) :: C(3)   !! Inverse matrix
    double precision             :: detinv,solve3(3),B(3,3)

    ! Calculate the inverse determinant of the matrix
    detinv = 1/(A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2)&
              - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1)&
              + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1))

    ! Calculate the inverse of the matrix
    B(1,1) = +detinv * (A(2,2)*A(3,3) - A(2,3)*A(3,2))
    B(2,1) = -detinv * (A(2,1)*A(3,3) - A(2,3)*A(3,1))
    B(3,1) = +detinv * (A(2,1)*A(3,2) - A(2,2)*A(3,1))
    B(1,2) = -detinv * (A(1,2)*A(3,3) - A(1,3)*A(3,2))
    B(2,2) = +detinv * (A(1,1)*A(3,3) - A(1,3)*A(3,1))
    B(3,2) = -detinv * (A(1,1)*A(3,2) - A(1,2)*A(3,1))
    B(1,3) = +detinv * (A(1,2)*A(2,3) - A(1,3)*A(2,2))
    B(2,3) = -detinv * (A(1,1)*A(2,3) - A(1,3)*A(2,1))
    B(3,3) = +detinv * (A(1,1)*A(2,2) - A(1,2)*A(2,1))

solve3=matmul(B,C)
  return
  end
