#!/bin/bash  

#SBATCH --job-name BSRT_batch
#
#SBATCH --ntasks=16
# 
#SBATCH -t 0-08:59
# 
#SBATCH -p debug 
# 
#SBATCH --mail-type=FAIL 
# 
#SBATCH --no-requeue 
# 
#SBATCH --export=ALL  
# 
#SBATCH --output=sh_BSRT.out
#
#SBATCH --error=sh_BSRT.err 


###############################################################
#                                                             #
#    Bourne shell script for submitting a parallel MPICH2 job #
#    to the DEBUG queue using the SBATCH command.             #
#                                                             #  
#    SLURM version of pbs.sh by Renier Viltres                #
#                                                             #
###############################################################

#     Remarks: A line beginning with # is a comment.
#	       A line beginning with #SBATCH is a SLURM directive.
#              SLURM directives must come first; any directives
#                 after the first executable statement are ignored.
#

YOUR_PATH_TO_CHANGE="<path to installation folder>"

cd ${YOUR_PATH_TO_CHANGE} 
export program=${YOUR_PATH_TO_CHANGE}/run

#---------------------------
mpirun ${program} > ${YOUR_PATH_TO_CHANGE}/OUT/mpi.out
#-------------------------------------

exit
