#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 17:06:07 2022

@author: marianne

Plot cross sections in PDF cubes 
install of weightedstats is needed
"""
import matplotlib.pyplot as plt
import numpy as np
import argparse
import weightedstats
import pandas as pd

parser = argparse.ArgumentParser(
    description='extract profile')
parser.add_argument('-save', help='path to save figures and csv profile, default is ./', default='./')
parser.add_argument('-out', help='path to the output files, default is OUT/', default='OUT/')
parser.add_argument('-alat', help='y coordinate of the first edge of the profile', type=float)
parser.add_argument('-alon', help='x coordinate of the first edge of the profile', type=float)
parser.add_argument('-blat', help='y coordinates of the second edge of the profile', type=float)
parser.add_argument('-blon', help='x coordinates of the second edge of the profile', type=float)
parser.add_argument('-parameter', help='value plotted (div, rot, inv, VX, VY)', default='inv')
parser.add_argument('-adjusty', help='give maxval for plotting in 10-9/yr', type = float)
parser.add_argument('-utm', help='epsg code for utm projection', type=str, default='32631')
parser.add_argument('-normal', help='normalization strategy for PDF either max or sum', type=str, default='sum')


args = parser.parse_args()


path = args.out
value = args.parameter

# load info for projection UTM31N
from pyproj import Transformer
wgs2utm = Transformer.from_crs("epsg:4326","epsg:"+args.utm)
utm2wgs = Transformer.from_crs("epsg:"+args.utm,"epsg:4326")


# read the param.out file
param = open(path+ '/param.out', "r")
lines = param.readlines()
lines = [line.strip() for line in lines]
lines = [line.split() for line in lines]

nbrPDF = tuple([int(lines[1][1]), int(lines[1][0]), int(lines[1][2])])
zone = tuple([float(t) for t in lines[2]])
bounds = tuple([float(t) for t in lines[6]])
if value == 'div':
    bounds = tuple([float(t) for t in lines[4]])
elif value == 'rot':
    bounds = tuple([float(t) for t in lines[5]])
elif value == 'VX':
    Vbounds = tuple([float(t) for t in lines[3]])
    bounds = (Vbounds[0]-Vbounds[1],Vbounds[0]+Vbounds[1])
elif value == 'VY':
    Vbounds = tuple([float(t) for t in lines[3]])
    bounds = (Vbounds[2]-Vbounds[3],Vbounds[2]+Vbounds[3])
    
xcoord = np.linspace(zone[0], zone[1], num=nbrPDF[0])
ycoord = np.linspace(zone[2], zone[3], num=nbrPDF[1])
lon, lat = np.meshgrid(xcoord,ycoord)

#find the nearest pixel for A
lonpx,latpx = wgs2utm.transform(args.alat,args.alon)
cubecoord=np.array([np.abs(lon-np.array(lonpx)/1000),np.abs(lat-np.array(latpx)/1000)])
t=cubecoord[0,:,:]+cubecoord[1,:,:]
x,y=np.where(t==t.min()) 
Ax=int(x[0])
Ay=int(y[0])

#find the nearest pixel for B
lonpx,latpx = wgs2utm.transform(args.blat,args.blon)
cubecoord=np.array([np.abs(lon-np.array(lonpx)/1000),np.abs(lat-np.array(latpx)/1000)])
t=cubecoord[0,:,:]+cubecoord[1,:,:]
x,y=np.where(t==t.min()) 
Bx=int(x[0])
By=int(y[0])

if all(x > nbrPDF[0] for x in (Ay,By)):
    print('bad X coordinate value')
    exit()
if all(y > nbrPDF[1] for y in (Ax,Bx)):
    print('bad Y coordinate value')
    exit()

print('plotting profile between', lon[Ax,Ay], lat[Ax,Ay], 'and', lon[Bx,By], lat[Bx,By],)

#define cross-section coordinates
#alpha = (Bx-Ax)/(By-Ay)
distance = np.sqrt((lon[Ax][Ay]-lon[Bx][By])**2+(lat[Ax][Ay]-lat[Bx][By])**2)
num = int(np.hypot(abs(Ay-By), abs(Ax-Bx)))
point = np.linspace(0,distance,num)
x, y = np.linspace(Ay,By, num), np.linspace(Ax, Bx, num)

vallong = np.loadtxt(path+ '/Posteriorinv.out', usecols = 2)
if value == 'div':
    vallong = np.loadtxt(path+ '/Posteriordiv.out', usecols = 2)
elif value == 'VX':
    vallong = np.loadtxt(path+ '/Posterior1.out', usecols = 2)
elif value == 'VY': 
    vallong = np.loadtxt(path+ '/Posterior2.out', usecols = 2)
elif value == 'rot':
    vallong = np.loadtxt(path+ '/Posteriorrot.out', usecols = 2)

data = np.loadtxt(path + "/DataUsed.out",  comments='#', usecols=(0,1,2,3))


# # swapaxes pour se remettre en geographique
cubedata = np.swapaxes(np.reshape(vallong, nbrPDF), 0, 1)

# # create the vector to sample the PDF
valval = np.linspace(bounds[0], bounds[1], num=nbrPDF[2])

# for maxmode plotting
# necessary to ignore values on the bounds that could be artificially high

valmax = np.argmax(cubedata[:,:,1:-1], axis=2)+1

pdfval = np.array([cubedata[x,y,1:-1] for x,y in zip(*(y.astype(int), x.astype(int)))])

if args.normal == 'max':
    print('ad hoc normalization of the PDF') # for normalization with max prob = 1
    pdfval = pdfval/np.max(pdfval, axis=1)[:,np.newaxis]
elif args.normal =='sum':
    print('ad hoc normalization of the PDF') # for normalization with similar area =1
    pdfval = pdfval/np.sum(pdfval, axis=1)[:,np.newaxis]


pval_max = [valval[np.argmax(cubedata[x,y,1:-1])+1]
            for x,y in zip(*(y.astype(int), x.astype(int)))]
pval_med = [weightedstats.numpy_weighted_median(valval[1:-1], cubedata[x,y,1:-1]) 
            for x,y in zip(*(y.astype(int), x.astype(int)))]
pval_moy = [np.average(valval[1:-1], weights=cubedata[x,y,1:-1]) 
            for x,y in zip(*(y.astype(int), x.astype(int)))]


#%%
fig, axes1 = plt.subplots(1,1)
#mycmap = plt.get_cmap('hot_r')
mycmap = plt.get_cmap('gist_heat_r')
plt_norm = 'linear'
if args.normal == 'max' :
    plt_norm = 'linear'

im= axes1.pcolormesh(point, valval[1:-1], pdfval.T, cmap=mycmap, shading='nearest', norm=plt_norm)
fig.colorbar(im, ax=axes1, label='Normalized PDF') 
    
axes1.plot([0,distance],[0,0],color = 'gray', linestyle='dashed')
axes1.plot(point,pval_max, color ='g', label='Max Mode')
axes1.plot(point,pval_med, color ='b', label='Median')
axes1.plot(point,pval_moy, color ='pink', label='Average')
if args.adjusty : 
    if args.parameter == 'div' or args.parameter == 'rot' :
        axes1.set_ylim(-args.adjusty*10**-9, args.adjusty*10**-9)
    else : 
        axes1.set_ylim(0, args.adjusty*10**-9)
axes1.set_xlabel("Distance (km)")
axes1.set_ylabel(value)
axes1.legend()

df = pd.DataFrame({'dist':point,'maxmode':pval_max,'median':pval_med,'average':pval_moy})
df.to_csv(args.save + '/profil_' + args.parameter + '.csv', sep=',', header=True, float_format='%.12f', index=False)

fig.tight_layout()
plt.savefig(args.save + '/csection_' + value + '_' + str(args.alon) + '-' + str(args.alat)+ str(args.blon) +'-' + str(args.blat) + '_full.pdf', format='pdf', bbox_inches='tight')

plt.show()
