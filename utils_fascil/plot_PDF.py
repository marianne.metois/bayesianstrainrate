#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 17:06:07 2022

@author: marianne

Plot PDFs for all outputs at one given pixel
install of weightedstats is needed
"""
import matplotlib.pyplot as plt
import numpy as np
import argparse
import weightedstats
import pandas as pd

parser = argparse.ArgumentParser(
    description='Plot PDFs for a parameter at one given pixel.')
parser.add_argument('-save', help='path to save figures, default is ./', default='./')
parser.add_argument('-out', help='path to the output files, default is OUT/', default='OUT/')
parser.add_argument('-parameter', help='value plotted (div, rot, inv, VX, VY)', default='inv')
parser.add_argument('-lon', help='x coordinate of the pixel in wgs84', type=float)
parser.add_argument('-lat', help='y coordinate of the pixel in wgs84', type=float)
parser.add_argument('-utm', help='epsg code for utm projection', type=str, default='32631')

args = parser.parse_args()


path = args.out
value = args.parameter

# load info for projection UTM31N
from pyproj import Transformer
wgs2utm = Transformer.from_crs("epsg:4326","epsg:"+args.utm)
utm2wgs = Transformer.from_crs("epsg:"+args.utm,"epsg:4326")


# read the param.out file
param = open(path+ '/param.out', "r")
lines = param.readlines()
lines = [line.strip() for line in lines]
lines = [line.split() for line in lines]

nbrPDF = tuple([int(lines[1][1]), int(lines[1][0]), int(lines[1][2])])
zone = tuple([float(t) for t in lines[2]])
bounds = tuple([float(t) for t in lines[6]])
value2='2nd Invariant'
if value == 'div':
    value2 = 'Divergence'
    bounds = tuple([float(t) for t in lines[4]])
elif value == 'rot':
    value2 = 'Vorticity'
    bounds = tuple([float(t) for t in lines[5]])
elif value == 'VX':
    Vbounds = tuple([float(t) for t in lines[3]])
    bounds = (Vbounds[0]-Vbounds[1],Vbounds[0]+Vbounds[1])
elif value == 'VY':
    Vbounds = tuple([float(t) for t in lines[3]])
    bounds = (Vbounds[2]-Vbounds[3],Vbounds[2]+Vbounds[3])

vallong = np.loadtxt(path+ '/Posteriorinv.out', usecols = 2)
if value == 'div':
    vallong = np.loadtxt(path+ '/Posteriordiv.out', usecols = 2)
elif value == 'VX':
    vallong = np.loadtxt(path+ '/Posterior1.out', usecols = 2)
elif value == 'VY': 
    vallong = np.loadtxt(path+ '/Posterior2.out', usecols = 2)
elif value == 'rot':
    vallong = np.loadtxt(path+ '/Posteriorrot.out', usecols = 2)

data = np.loadtxt(path + "/DataUsed.out",  comments='#', usecols=(0,1,2,3))


# swapaxes pour se remettre en geographique

cubedata = np.swapaxes(np.reshape(vallong, nbrPDF), 0, 1)

# pb: need to save the parameters.in to get bounds
# div -0.03 0.03
# create the vector to sample the PDF
xcoord = np.linspace(zone[0], zone[1], num=nbrPDF[0])
ycoord = np.linspace(zone[2], zone[3], num=nbrPDF[1])
lon, lat = np.meshgrid(xcoord,ycoord)
#lonp, latp = utm2wgs.transform(lat*1000,lon*1000)

#find the nearest pixel
lonpx,latpx = wgs2utm.transform(args.lat,args.lon)
cubecoord=np.array([np.abs(lon-np.array(lonpx)/1000),np.abs(lat-np.array(latpx)/1000)])
t=cubecoord[0,:,:]+cubecoord[1,:,:]
x,y=np.where(t==t.min()) 
x=int(x[0])
y=int(y[0])

# create the vector to sample the PDF
valval = np.linspace(bounds[0], bounds[1], num=nbrPDF[2])
if value == 'VX':
    valval = np.linspace(Vbounds[0]-Vbounds[1],Vbounds[0]+Vbounds[1], num=nbrPDF[2])
elif value == 'VY':
    valval = np.linspace(Vbounds[2]-Vbounds[3],Vbounds[2]+Vbounds[3], num=nbrPDF[2])

#%%
print('plotting pixel of coordinates', lon[x][y], lat[y][x])
fig, axes = plt.subplots(1, 1)
axes.bar(valval[1:-1], cubedata[x,y,1:-1], width=(np.abs(bounds[0])+np.abs(bounds[1]))/nbrPDF[2])
axes.vlines(valval[np.argmax(cubedata[x,y,1:-1])+1], 0, np.max(cubedata[x,y,1:-1]), colors='purple', label='Max mode')
axes.vlines(np.average(valval[1:-1], weights=cubedata[x,y,1:-1]), 0, np.max(cubedata[x,y,1:-1]), colors='green', label = 'Average')
axes.vlines(weightedstats.numpy_weighted_median(valval[1:-1], cubedata[x,y,1:-1]), 0, np.max(cubedata[x,y,1:-1]), colors='orange', label = 'Median')
axes.set_xlabel(value2)
axes.set_ylabel('number of models')
axes.legend()

fig.tight_layout()

plt.savefig(args.save + '/pdf_' + str(args.lon) + '_' + str(args.lat) + '.png', bbox_inches='tight')
df = pd.DataFrame({'values':valval[1:-1], 'models':cubedata[x,y,1:-1]})
df.to_csv(args.save + '/histo_' + args.parameter + str(args.lon)+'-'+str(args.lat)+'.csv', sep=',', header=True, float_format='%.12f', index=False)

plt.show()
