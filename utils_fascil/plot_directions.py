#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot principal strain/strain rate directions in map view for a selection of points
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as matplotlib
import argparse
import pygmt
import pandas as pd

parser = argparse.ArgumentParser(description="Plot principal strain direction in map view")
parser.add_argument("-save", help="path to save figures, default is ./", default='./')
parser.add_argument("-out", help="path to the output files, default is OUT/", default="OUT/")
parser.add_argument("-step", help="nbr of points for decimating grid, set to 0 if original grid is needed", type=int, default=30)
parser.add_argument("-opt", help="plotting option gmt or quiver", default='gmt')
parser.add_argument("-scale", help="scale for plotting principal direction, default is 0.2", type=float, default=0.2)
parser.add_argument('-utm', help='epsg code for utm projection', type=str, default='32631')

args = parser.parse_args()

option = args.opt
path = args.out

# load info for projection UTM31N
from pyproj import Transformer
wgs2utm = Transformer.from_crs("epsg:4326","epsg:"+args.utm)
utm2wgs = Transformer.from_crs("epsg:"+args.utm,"epsg:4326")

data = np.loadtxt(path + "/DataUsed.out", comments="#", usecols=(0, 1, 2, 3))
postlines = np.loadtxt(path + "/Posteriorphi.out")  # proba azimtuh first eigen vector
post1lines = np.loadtxt(path + "/Averagelambda1.out")  # first eigen value(norm1)
post2lines = np.loadtxt(path + "/Averagelambda2.out")  # second eigen value (norm2)

# read the param.out file
param = open(path + "/param.out", "r")
lines = param.readlines()
lines = [line.strip() for line in lines]
lines = [line.split() for line in lines]
nbrPDF = tuple([int(lines[1][1]), int(lines[1][0]), int(lines[1][2])])
zone = tuple([float(t) for t in lines[2]])

# nbr of bins for angle description (9 bins = 10° wide bins)
nbpointsangle = 9
anglePDF = list(nbrPDF)
anglePDF[2] = nbpointsangle
anglePDF = tuple(anglePDF)

#%%
# formattage directions principales
# swapaxes pour se remettre en geographique
# proba max angle
cubedata = np.swapaxes(np.reshape(postlines[:, 2], anglePDF), 0, 1)
xdata = np.swapaxes(np.reshape(postlines[:, 0], anglePDF), 0, 1)
ydata = np.swapaxes(np.reshape(postlines[:, 1], anglePDF), 0, 1)

# create the vector to sample the PDF
valmax = np.argmax(cubedata[:, :, :], axis=2)

l1data = np.swapaxes(np.reshape(post1lines[:, 2], anglePDF), 0, 1)
l2data = np.swapaxes(np.reshape(post2lines[:, 2], anglePDF), 0, 1)

l1M = np.zeros((list(anglePDF)[1], list(anglePDF)[0]))
l2M = np.zeros((list(anglePDF)[1], list(anglePDF)[0]))
angle = np.zeros((list(anglePDF)[1], list(anglePDF)[0]))

for i in range(0, list(anglePDF)[1]):
    for j in range(0, list(anglePDF)[0]):
        l1M[i, j] = l1data[i, j, valmax[i, j]]
        l2M[i, j] = l2data[i, j, valmax[i, j]]
        if l1M[i, j] < l2M[i, j]:
            angle[i, j] = 130 - 10 * valmax[i, j]
            l2M[i, j] = l1data[i, j, valmax[i, j]] # switch pour format velo
            l1M[i, j] = l2data[i, j, valmax[i, j]] # switch pour format velo
        else:
            angle[i, j] = 220 - 10 * valmax[i, j]

# convert cartesian coordinates to row,column numbers
xcoord = np.linspace(zone[0], zone[1], num=int(nbrPDF[0]))
ycoord = np.linspace(zone[2], zone[3], num=int(nbrPDF[1]))
lons, lats = np.meshgrid(xcoord, ycoord)

#%%
step = args.step
if step == 0 : 
     x50, y50 = np.meshgrid(np.array(list(range(0,nbrPDF[1],1))),
                       np.array(list(range(0,nbrPDF[0],1))))
else :
     x50, y50 = np.meshgrid(np.array(list(range(0,nbrPDF[1]-1,int(nbrPDF[1]/step)))), 
                       np.array(list(range(0,nbrPDF[0]-1,int(nbrPDF[0]/step)))))
x50 = x50.flatten()
y50 = y50.flatten()

select = np.array([np.stack((xdata[i,j,0], ydata[i,j,0], angle[i,j], l1M[i,j]*10**9, l2M[i,j]*10**9)) 
                   for i,j in zip(*(x50.astype(int), y50.astype(int)))])
coords = utm2wgs.transform(select[:,0]*1000,select[:,1]*1000)
select2 = np.stack((coords[1], coords[0], select[:,3], select[:,4], select[:,2])).T

df = pd.DataFrame({'lon':coords[1],'lat':coords[0],'e1':select[:,3],'e2':select[:,4], 'az':select[:,2]})
df.to_csv(args.save + '/paldir.csv', sep=',', header=True, float_format='%.6f', index=False)

#%%
#test plotting with pygmt
#**1**,\ **2**: longitude, latitude of station
#**3**: eps1, the most extensional eigenvalue of strain tensor,with extension taken positive.
#**4**: eps2, the most compressional eigenvalue of strain tensor,with extension taken positive.
#**5**: azimuth of eps2 in degrees CW from North.
scl=str(args.scale)

fig = pygmt.Figure()
pygmt.config(FONT_TITLE="10p,5", MAP_TITLE_OFFSET="1p", MAP_FRAME_TYPE="plain", FONT_LABEL="12p", FONT_ANNOT_PRIMARY="12p", MAP_FRAME_WIDTH="2p", MAP_FRAME_PEN='thin,black')
region=(np.min(coords[1]), np.max(coords[1]), np.min(coords[0]), np.max(coords[0]))

fig.basemap(region=region, projection='M30', frame=["a", "WSne"])
fig.coast(shorelines=True, water="skyblue", land="white", resolution="l")
fig.velo(data=select2, pen="0.6p,black",line=True,spec="x"+ scl,vector="4.0c+p1p+e+gred")

fig.velo(data=np.array([round(region[0])+5.5,round(region[3])-1.5,10,-10, 120],ndmin=2), pen="0.6p,red",line=True,spec="x"+ scl,vector="4.0c+p1p+e+gblack",)
fig.text(text="10 nstrain/yr", x=round(region[0])+5.5, y=round(region[3])-0.5, font="12p,Helvetica-Bold,black", fill="white")        

fig.savefig("./paldir.pdf", show=True)

fig.show()