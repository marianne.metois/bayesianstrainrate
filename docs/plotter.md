# Quick guide to Bstrain plotter 

The BstrainPlotter is an online tool hosted by OSUL [https://bstrainplotter.univ-lyon1.fr/](https://bstrainplotter.univ-lyon1.fr/) to visualize and explore 3D cubes containing the probability density functions associated with strain rate tensors.

We provide vizualisation for strain rates computed with Bstrain over California ([Pagani et al. 2021](https://doi.org/10.1029/2021JB021905)), Balkans and metropolitan France (unpublished). The service allows for plotting of average, median, or maximal probability mode of second invariant, divergence and vorticity of the strain rate tensor, together with principal directions and interpolated East and North velocities.

You can explore the Californian results. You can play along with results shown for France and Balkans but they are undocumented and still preliminary.

## Get map views

Click on the "start exploring" button on the welcome page. You end up with an empty planisphere centered on Congo. 

![image info](./pictures/welcome.png)

In the scrolling menus, you can choose between 

3 geographical zones :
- California,
- metropolitan France, 
- Balkans

5 parameters : 
- Divergence, given by {math}`d=tr(\mathbf{\dot\epsilon})=\dot\epsilon_{xx}+\dot\epsilon_{yy}`.
- Rotational, given by {math}`\omega=\frac{1}{2}(\partial_yv_x - \partial_xv_y)`
- Invariant, i.e. one definition of the second invariant of the strain rate tensor, given by {math}`I_2=\sqrt{\dot\epsilon_{xx}^2+\dot\epsilon_{yy}^2+2\dot\epsilon_{xy}^2}`
- East Velocity
- North Velocity

3 indicators : 
- the average of the PDF,
- the median of the PDF,
- the maximum mode of the PDF

Let's have a look at the median of the second invariant over California.

![image info](./pictures/I2calif.png)

You can play with the color bar at the bottom. By default, the map is masked based on a threshold value that you can also adjust. It is based on the standard deviation of the horizontal velocity for each pixel (in mm/yr) : the largest, the less resolved the inversion is. 

By clicking on the printer icon on the upper left corner of the map, you will automatically download a png view of the map together with the associated geotiff. The second band of the geotiff is the mask value described above. 

## Get pixel details

You can place the blue mark on the map on the pixel you are interested in and ask for a plot of the full PDF by clicking on "Plot Probability Density Function" blue button. The exact coordinates of the marker are updated on the left. The plot appears on the right and is not interactive so far. It shows the full PDF as an histogram with vertical lines standing for the average, median and maximum mode. This is useful to assess the gaussian shape and symmetry of the PDF. The graph can be downloaded as png, pdf, or directly in csv for future use. 

![image info](./pictures/PDF.png)

The "Plot Windrose" button will show the PDF of the principal directions of the strain rate tensor, using the windrose representation proposed by Pagani et al. 2021. By default, blue will stand for extension, and red for compression. The length of the bar are proportionnal to the strain rate value, while the intensity of the color stands for the normalized probability (dark red/blue is ~100\%). Black bars are the average principal directions. 

![image info](./pictures/windrose.png)

## Build profiles

By checking "Segment" on the left menu, you can place 2 blue markers on the maps that will materialize a profile line along which calculations will be made. The coordinates of both points can be manually adjusted on the left. Clicking the "Plot Cross Section Simple" button will show the value of the choosen parameter (I2 in our example) against distance along the profile. Median, Average and Maximum mode are plotted only. You can adjust the vertical scale with "Adjust Y value" on the left and recompile. The graph can be downloaded in png, pdf or csv. 

If the cross section is of interest, you can ask for a full view of the PDF by clicking the "Plot Cross Section Full" button, but carefull *this may be time consuming* depending on the cube size. The full cross section shows the normalized PDF color-coded along the profile and can also be downladed. In our example, we see three zones of high I2 corresponding the the San Andreas fault picking at more than 300 nstrain/yr, the Walker Lane at around 0.45 nstrain/yr and the Wasatch mountain range around 0.1 nstrain/yr.

![image info](./pictures/profcalif.png)

## Plot principal directions

This is the less advanced option of this plotter... But if you absolutely want to have a look at the principal directions of the strain rate tensor in map view, please use the last section of the menu on the left. You can adjust the scale of the arrows (scale) and the density of the grid you want (Step). This will compute a map of average principal directions that may be difficult to read. However, you can download the grid as a csv and make proper maps at home using your favorite plotter. 

![image info](./pictures/princdir.png)

## Units and conventions

- Velocities are expressed in mm/yr, while invariants of the strain rate tensors and the rotational are given in nstrain/yr. 

- In the plotter, negative divergence stands for shortening and will appear red, while extension is positive and blue.

- Negative rotation stands for a clockwise rotation and appears red, while positive rotation is anticlockwise and blue. 


