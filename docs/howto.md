# How to use

## Input files

The standard input data file must contain longitude, latitude, XX, YY, ve, erre, vn, errn 

Where XX and YY are not used in this version of the code, could be 7 8000 for instance

The path to the data file is hard coded in Regression2D.f90. 
The number of stations used must be modified in parameters.in (npointm) together with the size of the box for triangulation (longmin,longmax,latmin,latmax) and for the data used (longminda, longmaxda, latminda, latmaxda)

Note that longitude and latitude should be expressed in a projected system and given in **kilometers**, for instance the appropriate UTM projection for the zone. Velocities should be in **mm/yr** and displacements in **mm** to get proper adimensional strain rates or strain values. 

## Code behavior

In its original version, the code allows for inverting a noise parameter called sigma that scales uniformly the uncertainties on the observations. 

Bounds for this factor can be set in parameters.in. We recommand using a maximum value for sigma of 1.1 if you trust your data and uncertainties.

Recursive prior option is not fully operationnal. Do not uncomment.

The size of each prior can (and must) be adjusted in parameters.in to be as close as possible as the expected pattern.

Example : adjust the mean1 and theta1 parameters to describe the prior on the northern component of the velocity by taking

```{math} 
mean1 = \frac{V_{max}+V_{min}}{2}

theta1 = \frac{(V_{max}-V_{min})\times 1.2}{2}
```
 if you want to allow 20\% deviation from the observed bounds.  

## Output files

All output are written as .out files

- Nb\_cells.out : Number of cells in function of iterations. Useful for checking the convergence of the algorithm

- Evidence.out : Posterior distribution on the number of cells

- ML\_sigma.out : Posterior distribution on data noise

- conv\_sigma.out : Evolution of data noise with iteration

- Average.out : Average solution map (X, Y, average Vx, average Vy, average divergence, average vorticity, average 2nd inv). Carefull, average may be misleading.

- Standard\_deviation.out : Map of model uncertainty

- Median.out : Median map

- Maximum.out : Maximum map

- PosteriorXXX.out : Cube of the posterior distribution for the XXX parameter. Store as i lines with X, Y, value for each pixel, i beign the number of bins chosen to describe the PDF (nvd parameter in parameters.in)

- mpi.out : statistics of the chains written when running the code in parallel

- out.out : Error messages

- param.out : list of parameters from parameters.in for plotting python scripts

```{note}
Statistics of the chains are written interactively in OUT/mpi.out when running the code in parallel. If code is run directly from `./run`, you may generate a fake mpi.out file for using plot utilities. This fake file should contain the last part of the print screen messages.
```

