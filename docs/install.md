# Install Bstrain

## Architecture 

- Regression2D.f90 : main program

- parameters.in : gather many parameters that need to be adjusted. Warning, some may remain hard-coded in Regression2D.f90 at this stage

- qhull and nn directories : libraries called for building Delaunay triangulation

- fmodel.f90 : computes the forward model for each proposed triangulation

## Requirements and Compiling

mpifortran is required for compilation.

```{note}
***Compilation is needed after any change to parameters.in**
```

```bash
make clean & make
```

This produces an executable which is called run.

It can be run by itself on one processor (by typing `./run`), or in parallel with  `qsub pbs.sh` if you work locally on geodcalctb Lyon 1 cluster.

```{warning}
Make sure that paths are correct in pbs.sh
```

