! This progranm does a Transdimensional 2D regression with
! the reversible jump algorithm
! Thomas Bodin June 2011
!----------------------------------------------------------------------------
! inputs are: 

! velocity or displacement files : lon, lat , char, char, VN, errN, VE, errE

!----------------------------------------------------------------------------
! outputs are:

! 'NB_cells.out' number of cells in function of iterations. usefulle for 
!  checking convergence of the algorithm
! 'Evidence.out' posterior distribution on the number of cells
! 'ML_sigma.out' posterior distribution on data noise, i.e. marginal likelihood on sigma
! 'Average.out' Average solution map 
! 'Standard_deviation.out' Map of model uncertainty
! 'Median.out' Median map
! 'Maximum.out' Maximum map
! 'posterior.out' full 3D volume giving the probability density at each pixel of the map

program Regression2D
  implicit none
  include 'mpif.h'
  include 'parameters.in'
  
!Interface for the solve3 function that manipulates arrays

  interface
    function solve3(AA,SM)
    double precision, intent(in) :: AA(3,3)  !! Matrix
    double precision, intent(in) :: SM(3)  !! Inverse matrix
    double precision             :: detinv,solve3(3),B(3,3)
    end function
  end interface

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!***************************************************************
! DECLARATIONS OF VARIABLES
!****************************************************************
  !real mean, theta
  integer           vertices(3,nt_max)
  integer           neighbour(3,nt_max)
  integer           nnn(ncell_max+1)
  integer           nnlist(nmax)
  integer           ntwork(nmax)
  integer           walk
  integer           worki1(nv_max)
  integer           worki2(nv_max)
  integer           worki3(nv_max)
  integer           dmode,b,d
  integer           nt,node
  double precision tetaS,tetaR,phiS,phiR,c,alpha,angle,pos1(3),pos2(3),SM(3),X1(3),X2(3),val1,val2,&
dVxdx,dVxdy,dVydx,dVydy,pos3(3),pos4(3),phi,axisrot

!
  real dobs1(npointm,2),dobs2(npointm,2),dest1(npointm),dest2(npointm),dobsmin1,dobsmax1,dobsmin2,dobsmax2

  real x_min,x_max,y_min,y_max,theta1max,theta2max,maxbound,minbound,poscentral1,poscentral2
  double precision        location(npointm,2)
  character*8   cha
  integer maxx1,maxx2,npoint,iface,kk,loc

  real , EXTERNAL    ::    gasdev,logprior,ran3
  real log,sqrt
  real t1,t2,lgprior_prop,lgprior_pprop,sigma,sigma_prop,lgsigma,sigav
  real lat,long,TT1,TT2,TT2a,TT2b,TT3 ! test by marianne LX to double precision
  double precision l1,l2,l3,l4,l5,l6,l7,l8
  integer Evidence(ncell_max),EvidenceS(ncell_max),WA(win),w,as,ML_sigma(180),ML_sigmas(180)
  real AR(3),ARB(3),ARM(3),ARV(3),ARD(3),AR1p(3),AR2p(3),AR1v(3),AR2v(3)
  real like,like_prop,like_pprop
  real vect(2),median1(nvp,nvt),median2(nvp,nvt),central1_new(nvp,nvt),central2_new(nvp,nvt),central1_old(nvp,nvt),&
central2_old(nvp,nvt),central(2),central_previous(2)
  real vectp(2)
  real distmin,ncellav,winav,winva,depth
  integer p,pbis1,pbis2,pp1,pp2,aaa,vert(3)
  integer nrr,nr,npcr,sampletotal
  integer ount,thin
  integer indx,ncell,ncell_prop,nrec,nsou,i,j,s,r,sample,ind,a,a2,nc,it
  
  !  Voro(x y v1 v2, index pour la cellule)
  double precision Voro(4,ncell_max),Voro_prop(4,ncell_max),Voro_pprop(4,ncell_max),AA1(3,3),AA2(3,3)
  
  double precision p1,p2,prob,RT(2,2),E(2,2),lambda(2,2),nonzero
  !
  real AVE1(nvp,nvt),VAR1(nvp,nvt),AVE2(nvp,nvt),VAR2(nvp,nvt),AVES1(nvp,nvt),VARS1(nvp,nvt),AVES2(nvp,nvt),VARS2(nvp,nvt),&
ARES(nvt,nvp),AVEDIV(nvp,nvt),AVEROT(nvp,nvt),AVEDIVS(nvp,nvt),AVEROTS(nvp,nvt),INV2(nvp,nvt),&
INV2S(nvp,nvt),LAMBDA1(nvp,nvt,9),LAMBDA1S(nvp,nvt,9),&!,AVEMOM(nvp,nvt),AVEMOMS(nvp,nvt)
LAMBDA2(nvp,nvt,9),LAMBDA2S(nvp,nvt,9)
  
  real AVW(nsample+burn_in-win,2),&
AVWS(nsample+burn_in-win,2),maxmap1(nvp,nvt),&
maxmap2(nvp,nvt),conv_sigma(nsample+burn_in)

  integer postdiv(nvp,nvt,nvd),postrot(nvp,nvt,nvd),postinv(nvp,nvt,nvd),&
postsdiv(nvp,nvt,nvd),postsrot(nvp,nvt,nvd),postsinv(nvp,nvt,nvd),postmom(nvp,nvt,nvd),postsmom(nvp,nvt,nvd)
  integer post1(nvp,nvt,nvd),post2(nvp,nvt,nvd),posts1(nvp,nvt,nvd),posts2(nvp,nvt,nvd),postslambda1(nvp,nvt,nvd),&
postslambda2(nvp,nvt,nvd),postphi(nvp,nvt,nvd),postsphi(nvp,nvt,nvd),post1axis(nvp,nvt,nvd),posts1axis(nvp,nvt,nvd),&
post2axis(nvp,nvt,nvd),posts2axis(nvp,nvt,nvd)



  
  real v,n,t,density(nvp,nvt),densitys(nvp,nvt)
  double precision distVP(ncell_max)

  integer np,ii,jj,jj1,jj2,jj3,jj4,jj5,jj6,jj7,jj10,jj11,istart,iend,jstart,jend,k

  integer birth,death,move,flag
  real u
  integer prop(3)

  logical          accept,DRv,DRp
  logical           ldummy(ncell_max),boolval
  
  !for MPI
  integer ra,rank, nbproc, ierror, tag, status(MPI_STATUS_SIZE)

  character filename*13, number*4
  1000  format(I4)


  CALL cpu_time(t1) 

  ! Start Parrallelization
  call MPI_INIT(ierror)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, nbproc, ierror)
  call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierror)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !***************************************************************
  ! Initailaization of Variables.
  !***************************************************************


  !Random seed different for each parrallel chain
  ra=rank

  !These enables to activate the Delayed rejection on Velocity or POsition
  DRp=.true.
  DRv=.true.

  lat = (latmaxd - latmind)/real(nvt-1)
  long = (longmaxd - longmind)/real(nvp-1)
  depth = 2*theta1/real(nvd)
  
  conv_sigma=0  
  ML_sigma=0
  ncellav=0
  Evidence=0
  EvidenceS=0
  AR=1
  ARV=1
  ARM=1
  ARB=1
  ARD=1
  AR1p=1
  AR2p=1
  AR1v=1
  AR2v=1
  nonzero=0.00000000000000000001 !(10⁻20)
  prob=1 
  b=0
  d=0
sampletotal = nsample+burn_in

axisrot=pi*axisangle/180
VAR1=0
VAR2=0
AVE1=0
AVE2=0
AVEDIV=0
AVEROT=0
!AVEMOM=0
INV2=0
LAMBDA1=0
LAMBDA2=0
density=0
densitys=0
post1=0
post2=0
post1axis=0
post1axis=0
postdiv=0
postrot=0
postinv=0
postmom=0
maxmap1=0
maxmap2=0
ount=0!ount=count
sample=0
thin=0
prop=0
w=0
WA=0
AVW=0
dobs1=0
dobs2=0
!****************************************************** 
!******************************************************
!* Get the Observed DATA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! NOTE : the form of the observed data are independent of the main 
! program, and hence this could be placed in the subroutine "forward model".
! However, the Forward model is run a large number of times, so here we read 
! the observed data (+ locations of regression points) and pass the infos to 
! fmodel.f90 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!In case of recursive computation, charge the "central" file from previous run
if (recursiveprior==1) then
        open(1000, file='OUT/central_old.out', status='old')

        DO i=1,nvp
                DO j=1,nvt
                        read(1000,*) poscentral1,poscentral2,central1_old(i,j),central2_old(i,j)
                        !if ((abs(central1_old(j,nvt-i+1)) .GT. 50) .or. (abs(central2_old(j,nvt-i+1)) .GT. 50)) then
                        !        write(*,*)central1_old(j,nvt-i+1),central2_old(j,nvt-i+1)
                        !end if
                END DO
        END DO
        close(1000)
end if
  !!!!open(1, file='D.dat', status='old')
  !!!!!open(1, file='global_set.dat', status='old')
  
  !LES DATAs!!
        !open(1, file='fulldata-balkans.dat', status='old')
        !open(1, file='midas_IGS14.dat', status='old')
        open(1, file='data/vnul_noise_projcut.txt', status='old')
  if (rank==0) open(3,file='locations.txt',status='replace')
  if (rank==0) open(13,file='OUT/DataUsed.out',status='replace')
  !sou(:,2) is longitude and sou(:,1) is latitude
  dobsmin1=10000000
  dobsmax1=-10000000
  dobsmin2=10000000
  dobsmax2=-10000000
  y_min=10000000
  y_max=-10000000
  x_min=10000000
  x_max=-10000000

npoint=0  

! ATTENTION LECTURE DE DATA SUR l'ENSEMBLE DES LIGNES DU FICHIER DATA.
! SELECTION AVEC DES IF 

 ! 
!  npointm est la taille du fichier data
DO i=1,npointm
          !read(1,*) location(i,1),location(i,2),dobs(i,1),dobs(i,2),cha
          read(1,*) l1,l2,l3,l4,l5,l6,l7,l8 !,cha
        !write(*,*)i
!  1 et 2 long lat; 5 valeur selon x ; 6 erreur selon x ; 7 et 8 valeur et
! erreur selon y.
! long lat time NB_sampleUsed data uncertainty
        !if (l1<-180) l1 = l1 + 360
        !if (l1>180) l1 = l1 - 360
! ADAPT FOR mm/year, DEPENDING ON THE SOURCE FILE
!        l5=l5*1000
!        l6=l6*1000
!        l7=l7*1000
!        l8=l8*1000
         !write(*,*)l1,l2,l3,l4,l5,l6!l3,l4,l4/l3! l1,l2,l3,l4,cha


!  TRIE LES DONNEES

        if ((l1>longminda).and.(l1<longmaxda).and.(l2<latmaxda).and.(l2>latminda)) then
 
!write(*,*)longmind,longmaxd,l1,l2
  !      if(abs(l5)<data_amplitude_max .and. abs(l7)<data_amplitude_max) then
        if(l3>=data_length_min) then
  !      if((1+(l4/2)/365.25)/l3>ratio_min) then
  !      if(l6<data_stddev_max .and. l8<data_stddev_max) then
  !             if((l2>15).or.(l2<-15)) then 
                npoint=npoint+1
                  location(npoint,1)=l1
                location(npoint,2)=l2
        
                ! OBESRVATIONS DANS LE VECTEUR dobs
                dobs1(npoint,1)=l5
                dobs1(npoint,2)=l6
                dobs2(npoint,1)=l7
                dobs2(npoint,2)=l8
                
                if (rank==0) write(13,*)l1,l2,l5,l6,l7,l8!,cha 

                ! 
                dobs1(npoint,2)=1/dobs1(npoint,2)
                dobs2(npoint,2)=1/dobs2(npoint,2)
                
                !dobs(npoint,2)=1/(0.01+abs(dobs(npoint,1)))
                !  
                if (dobs1(npoint,1)<dobsmin1) dobsmin1=dobs1(npoint,1)        
                if (dobs1(npoint,1)>dobsmax1) dobsmax1=dobs1(npoint,1)
                if (dobs2(npoint,1)<dobsmin2) dobsmin2=dobs2(npoint,1)        
                if (dobs2(npoint,1)>dobsmax2) dobsmax2=dobs2(npoint,1)
                
                
                if (location(npoint,1)<y_min) y_min=location(npoint,1)        
                if (location(npoint,1)>y_max) y_max=location(npoint,1)
                if (location(npoint,2)<x_min)  x_min=location(npoint,2)        
                if (location(npoint,2)>x_max)  x_max=location(npoint,2)
                if (rank==0) write(3,*)location(npoint,1),location(npoint,2)
   !write(*,*)npoint,location(npoint,1),location(npoint,2),dobs(npoint,1),dobs(npoint,2)
   !  end if
    end if   
   !  end if   
   !  end if
   !  end if
     end if   
  !write(*,*)dobs(i,:)
  END DO
  close(1)
  if (rank==0) close(3)
  if (rank==0) close(13)
if (rank==0) then
write(*,*)'nb of points in the box', npoint
write(*,*)'min1',dobsmin1
write(*,*)'max1',dobsmax1
write(*,*)'min2',dobsmin2
write(*,*)'max2',dobsmax2,10**(-20)
write(*,*)'x bounds',y_min,y_max
write(*,*)'y bounds',x_min,x_max

!mean=(dobsmax+dobsmin)/2
!theta=(dobsmax-dobsmin)/2

write(*,*)mean1,'=/-',theta1
write(*,*)mean2,'=/-',theta2
endif

!******************************************************
! DEFINE RANDOMLY THE INITIAL MODEL
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!* Initial location of the cells (V(:,1:2))
!* Initial value of the centre os Voronoi cells V(:,3)
! (Of course the initial model is going to be different 
! for each chain since the random seed is different.)


!* Initilalise sigma (data noise value)
sigma = 1.0! sigma_min+ran3(ra)*(sigma_max-sigma_min)

if (rank==0) write(*,*)'INITIALIZING MODEL ...'

IF (Scratch == 0 ) THEN ! START FROM ALREADY DEFINED MODELS (NO NEED FOR BURN-IN)
        write(number,1000)rank
        filename=number//'model.dat'
        open(rank*10,file='Store/'//filename,status='old') 
        write(*,*)'file', rank 
                            read(rank*10,*)j
                            if (j.ne.nbproc) then  
                                  write(*,*) 'CHECK NB OF CHAINS VS NB OF CHAINS IN /STORE'
                                  stop
                            endif
                            read(rank*10,*)ncell
                            do i=1,ncell
                                    read(rank*10,*)voro(:,i)
                            enddo
        close(rank*10)
        write(*,*)'INITIAL MODELS READ FROM /STORE'

ELSE ! START FROM SCRATCH (NEED FOR BURN-IN)
        !* Initial number of cells
        !ncell=int(ncell_min+ran3(ra)*(ncell_ma-ncell_min))
        

!                    location(npoint,1)=l1
!                location(npoint,2)=l2
!                dobs(npoint,1)=l5
!                dobs(npoint,2)=l6

        ncell=0!int(ncell_min+ran3(ra)*(100-ncell_min))

! CORNER INITIALIZATION : ADDING NECESSARY NODES ON THE VERTICES OF THE RECTANGULAR AREA 
        ncell=ncell+4
        Voro(1,1)=longmin
        Voro(1,2)=longmin
        Voro(1,3)=longmax
        Voro(1,4)=longmax
        Voro(2,1)=latmin
        Voro(2,2)=latmax
        Voro(2,3)=latmin
        Voro(2,4)=latmax
        DO i=1,4
                Voro(3,i)=mean1
                Voro(4,i)=mean2
        ENDDO
! In case a corner is on position (0,0)
        if ((longmin==0) .and. (latmin==0)) then
                Voro(3,1)=0.01
                Voro(4,1)=0.01
        end if

        DO i=1,npoint
                !We place them randomly
                !Choose randomly whether we shoudl include this point
                if (ran3(ra)<ratio_init)then
                
                ncell=ncell+1
                
                !Voro(1:2,i) = [longmin+ran3(ra)*(longmax-longmin),latmin+ran3(ra)*(latmax-latmin)]
                Voro(1:2,ncell) = [location(i,1),location(i,2)]
                
                !remove duplicate---------
                if (ncell>1)then
                        do j=1,ncell-1
                        if ((Voro(1,ncell)==Voro(1,j)).and.(Voro(2,ncell)==Voro(2,j))) then
                        Voro(1:2,ncell) = [longmin+ran3(ra)*(longmax-longmin),latmin+ran3(ra)*(latmax-latmin)]
                        endif
                        enddo
                endif

!write(*,*)!---------
!write(*,*)Voro(1:2,i)
                !-------------------------
                 
                !Voro(1:2,i) = [0+ran3(ra)*(23-0),33+ran3(ra)*(57-33)]
                ! random initial value for each cells 
                
                !Voro(3,i) = mean - theta +2*ran3(ra)*theta !+ GASDEV(bb)*0.5
                if (recursiveprior==0) then  
                        aaa=0
                        !IF VALUE OUTSIDE THE ALLOWED RANGE
                        if (dobs1(i,1).ge.(mean1+theta1)) then
                                Voro(3,ncell)=mean1+0.99*theta1 !
                        elseif (dobs1(i,1).le.(mean1-theta1)) then
                                Voro(3,ncell)=mean1-0.99*theta1 ! 
                        else !IF VALUE IS INSIDE THE RANGE
                                do while(aaa==0) 
                                        ! voro=model et dobs=data        
                                        Voro(3,ncell)=dobs1(i,1)+GASDEV(ra)*sigma*dobs1(i,2)
                                        if ((Voro(3,ncell)<(mean1+theta1)).and.(Voro(3,ncell)>(mean1-theta1))) aaa=1 !
                                enddo                        
                        endif
                        aaa=0
                        !IF VALUE OUTSIDE THE ALLOWED RANGE
                        if (dobs2(i,1).ge.(mean2+theta2)) then
                                Voro(4,ncell)=mean2+0.99*theta2 !
                        elseif (dobs2(i,1).le.(mean2-theta2)) then
                                Voro(4,ncell)=mean2-0.99*theta2 ! 
                        else !IF VALUE IS INSIDE THE RANGE
                                do while(aaa==0) 
                                        ! voro=model et dobs=data        
                                        Voro(4,ncell)=dobs2(i,1)+GASDEV(ra)*sigma*dobs2(i,2)
                                        if ((Voro(4,ncell)<(mean2+theta2)).and.(Voro(4,ncell)>(mean2-theta2))) aaa=1 !
                                enddo                        
                        endif
                else
                        central=find_central(Voro(1,ncell),Voro(2,ncell))
                        aaa=0
                        !IF VALUE OUTSIDE THE ALLOWED RANGE
                        if (dobs1(i,1).ge.(central(1)+theta1)) then
                                Voro(3,ncell)=central(1)+0.99*theta1 !
                        elseif (dobs1(i,1).le.(central(1)-theta1)) then
                                Voro(3,ncell)=central(1)-0.99*theta1 ! 
                        else !IF VALUE IS INSIDE THE RANGE
                                do while(aaa==0)
                                        ! voro=model et dobs=data        
                                        Voro(3,ncell)=dobs1(i,1)+GASDEV(ra)*sigma*dobs1(i,2)
                                        if ((Voro(3,ncell)<(central(1)+theta1)).and.(Voro(3,ncell)>(central(1)-theta1))) aaa=1 !
                                enddo
                        endif
                        aaa=0
                        !IF VALUE OUTSIDE THE ALLOWED RANGE
                        if (dobs2(i,1).ge.(central(2)+theta2)) then
                                Voro(4,ncell)=central(2)+0.99*theta2 !
                        elseif (dobs2(i,1).le.(central(2)-theta2)) then
                                Voro(4,ncell)=central(2)-0.99*theta2 ! 
                        else !IF VALUE IS INSIDE THE RANGE
                                do while(aaa==0)
                                        ! voro=model et dobs=data        
                                        Voro(4,ncell)=dobs2(i,1)+GASDEV(ra)*sigma*dobs2(i,2)
                                        if ((Voro(4,ncell)<(central(2)+theta2)).and.(Voro(4,ncell)>(central(2)-theta2))) aaa=1 !
                                enddo
                        endif
                endif

                endif
                !write(*,*)'-------------'
                !write(*,*)location(i,1),location(i,2),Voro(1,i),Voro(2,i)
        if (ncell==ncell_ma) exit
        END DO
ENDIF  

if (rank ==0) then
        DO i=1,ncell
                 write(*,*) Voro(1,i),Voro(2,i)
        ENDDO
endif

        
!stop
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!******************************************************
!if (rank==0) write(*,*)'Computing initial likelihood ...'


!******************************************************
! Get likelihood for the Initial Model 
!******************************************************

!compute estimated data from the initial model
! Call the forward model

! modif fmodel. Calcule dest . 
call fmodel(voro,ncell,location,npoint,dest1,dest2,npointm,ncell_max,nt_max,nmax,nv_max,eps)

!VERY IMPORTANT NOTE: like is the least square misfit.
! The likelihood is exp(-like)
   like=0
   ! COMMENTED FOR THE SAKE OF TESTING
   DO i=1,npoint
        ! write(*,*)location(i,:),dobs(i,1),dest(i)
         if (norm==2)then
                like = like + (((dobs1(i,1)-dest1(i))*dobs1(i,2))**2)/(2*(sigma**2)) + &
(((dobs2(i,1)-dest2(i))*dobs2(i,2))**2)/(2*(sigma**2))
                !write(*,*)i,like,dobs1(i,1),dest1(i),dobs1(i,2),dobs2(i,1),dest2(i),dobs2(i,2),sigma
        else if (norm==1) then
                like = like + (abs((dobs1(i,1)-dest1(i))*dobs1(i,2)))/(sigma) + (abs((dobs2(i,1)-dest2(i))*dobs2(i,2)))/(sigma)

                !write(*,*)i,dobs(i,1),dest(i)!,dobs(i,2),sigma
        else if (norm==3) then
                like = like + log(pi*sigma/dobs1(i,2)) + log(1+((dobs1(i,1)-dest1(i))*dobs1(i,2)/sigma)**2) + &
log(pi*sigma/dobs2(i,2)) + log(1+((dobs2(i,1)-dest2(i))*dobs2(i,2)/sigma)**2) 
        endif
   END DO
   write(*,*)'proc',rank,'Initial misfit',like*(2*(sigma**2)),'nbcell',ncell
   write(*,*)


   
!**********************************************************************




!***********************************************************************
! Start the sampling of the posterior distribution
!***********************************************************************


DO WHILE (sample.LT.nsample)
ount=ount+1
Voro_prop = Voro
ncell_prop = ncell
a=0
birth=0
death=0
move=0

conv_sigma(ount)=sigma
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!* propose a new sigma (i.e. noise parameter)
sigma_prop = sigma + GASDEV(ra)*ps
!*********************************

!*********************************
!* Check its bounds. If outside the bouds of the prior, then this 
! proposal will be rejected
as=1;
if ((sigma_prop < sigma_min).or.(sigma_prop > sigma_max)) then
        as=0
end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!*********************************
! Every other iteration, move a cell 
!(i.e. ethier ove, add, or remove)
IF (mod(ount,2).EQ.0) THEN

u=ran3(ra)
 if (u<0.33) then !add a cell
        birth=1
        !check for boudary, not allowed more than ncell_max
        if (ncell.ne.ncell_ma) a=1 
 else if (u<0.66) then         !remove a cell
        death=1
        !check for boudary, not allowed less than ncell_min
        if (ncell.ne.ncell_min) a=1
 else ! move a cell
         move=1 
 end if  

!--------------------------------------------
! now depending on move type update the model
   if (birth==1) then
         
        if (a==1) then         !we're adding a cell
                ncell_prop = ncell+1;
                !generate a new location
                Voro_prop(1:2,ncell_prop) = [longmin+ran3(ra)*(longmax-longmin),latmin+ran3(ra)*(latmax-latmin)]
        
                if (recursiveprior==0) then
        !         where is the value of the old cell?
                        call delaun (Voro(1:2,:),ncell,neighbour,vertices,nt,nt_max,&
                                worki1,worki2,worki3,eps,nv_max,&
                                0,ldummy,0,0,0)
                
                        call build_nv(ncell,vertices,nt,ncell_max,nmax,&
                                neighbour,nnn,nnlist,ntwork)
                        loc = 1                
                        p1=Voro_prop(1,ncell_prop)
                        p2=Voro_prop(2,ncell_prop)
                        call Triloc_del(p1,p2,Voro(1:2,:),vertices,neighbour,loc,eps,boolval,k,iface)
                        vert=vertices(1:3,loc)
                        pos1=Voro(1,vert(1:3))
                        pos2=Voro(2,vert(1:3))
                        pos3=Voro(3,vert(1:3))
                        pos4=Voro(4,vert(1:3))
                        SM=(/ -1,-1,-1 /)
                        AA1(1:3,1)=pos1
                        AA1(1:3,2)=pos2
                        AA1(1:3,3)=pos3
                        X1=solve3(AA1,SM)
                        val1=(-p1*X1(1)-p2*X1(2)-1)/X1(3)

                        AA2(1:3,1)=pos1
                        AA2(1:3,2)=pos2
                        AA2(1:3,3)=pos4
                        X2=solve3(AA2,SM)
                        val2=(-p1*X2(1)-p2*X2(2)-1)/X2(3)



                        !value in the new cell ! 
                        Voro_prop(3,ncell_prop) = val1 + GASDEV(ra)*sigmav 
                        Voro_prop(4,ncell_prop) = val2 + GASDEV(ra)*sigmav 

                        !write(*,*)1,p1,p2,val1,val2,pos1,pos2,pos3,pos4,vert,Voro_prop(3,ncell_prop),Voro_prop(4,ncell_prop)
                        
                        prob=(1/(sigmav*sqrt(2*pi)))*exp(-(val1-Voro_prop(3,ncell_prop))**2/(2*sigmav**2))*&
(1/(sigmav*sqrt(2*pi)))*exp(-(val2-Voro_prop(4,ncell_prop))**2/(2*sigmav**2))
                        !write(*,*)1,"n° noeuds",vert,"pos noeuds",pos1,pos2,"val noeuds",pos3,pos4&
!,"val prop",Voro_prop(3,ncell_prop),Voro_prop(4,ncell_prop),"ancienne val",val1,val2,1/(4*theta1*theta2*prob)
                else
                        call delaun(Voro(1:2,:),ncell,neighbour,vertices,nt,nt_max,&
                                worki1,worki2,worki3,eps,nv_max,&
                                0,ldummy,0,0,0)

                        call build_nv(ncell,vertices,nt,ncell_max,nmax,&
                        neighbour,nnn,nnlist,ntwork)
                        node=1
                        call find_node2D([Voro_prop(1,ncell_prop),Voro_prop(2,ncell_prop)]&
                        ,node,voro(1:2,:),nnn,nnlist,walk)

                        central_previous=find_central(Voro(1,node),Voro(2,node))
                        central=find_central(Voro_prop(1,ncell_prop),Voro_prop(2,ncell_prop))
                        Voro_prop(3,ncell_prop) = Voro(3,node)+central(1)-central_previous(1)+GASDEV(ra)*sigmav
                        Voro_prop(4,ncell_prop) = Voro(4,node)-central_previous(2)+GASDEV(ra)*sigmav

                        prob=(1/(sigmav*sqrt(2*pi)))*exp(-(Voro(3,node)-Voro_prop(3,ncell_prop)+central(1)-central_previous(1))**2/&
(2*sigmav**2))*(1/(sigmav*sqrt(2*pi)))*exp(-(Voro(4,node)-Voro_prop(4,ncell_prop)-central(2)+central_previous(2))**2/(2*sigmav**2))
                end if
!  IMPORTANT ATTENTION PROB=prob1*prob2 
                
                if (recursiveprior==0) then
                        if (abs(mean1-Voro_prop(3,ncell_prop))>theta1 .or. abs(mean2-Voro_prop(4,ncell_prop))>theta2 ) a=0 !Colin
                else
                        central=find_central(Voro_prop(1,ncell_prop),Voro_prop(2,ncell_prop))
                        if (abs(central(1)-Voro_prop(3,ncell_prop))>theta1 .or. abs(central(2)-Voro_prop(4,ncell_prop))>theta2 ) a=0 !Colin
                end if
        end if !a==1

   else if (death==1) then 
   !write(*,*)'death'
        if (a==1) then
                !we've lost a cell
                ncell_prop = ncell-1;
                !choose a cell to delete
                ind=4
                do while(ind==4)
                        ind = ceiling(ran3(ra)*(ncell-4))+4
                enddo
                !replace the deleted one by the last one
                Voro_prop(1:4,ind) = Voro_prop(1:4,ncell) !                
                
                if (recursiveprior==0) then
                        ! compute proposed delauney triangulation : nnn_prop,nnlist_prop
                        call delaun (voro_prop(1:2,:),ncell_prop,neighbour,vertices,nt,nt_max,&
                                worki1,worki2,worki3,eps,nv_max,&
                                0,ldummy,0,0,0)
                
                        call build_nv(ncell_prop,vertices,nt,ncell_max,nmax,&
                                neighbour,nnn,nnlist,ntwork)
                        loc = 1
                        !what is the value of voro(1:2,ind) in the new tesselation
                        p1=Voro(1,ind)
                        p2=Voro(2,ind)
                        call Triloc_del(p1,p2,Voro_prop(1:2,:),vertices,neighbour,loc,eps,boolval,k,iface)
                        vert=vertices(1:3,loc)
                        pos1=Voro_prop(1,vert(1:3))
                        pos2=Voro_prop(2,vert(1:3))
                        pos3=Voro_prop(3,vert(1:3))
                        pos4=Voro_prop(4,vert(1:3))
                        SM=(/ -1,-1,-1 /)
                        AA1(1:3,1)=pos1
                        AA1(1:3,2)=pos2
                        AA1(1:3,3)=pos3
                        X1=solve3(AA1,SM)
                        val1=(-p1*X1(1)-p2*X1(2)-1)/X1(3)

                        AA2(1:3,1)=pos1
                        AA2(1:3,2)=pos2
                        AA2(1:3,3)=pos4
                        X2=solve3(AA2,SM)
                        val2=(-p1*X2(1)-p2*X2(2)-1)/X2(3)

                        prob=(1/(sigmav*sqrt(2*pi)))*exp(-(voro(3,ind)-val1)**2/(2*sigmav**2))*&
(1/(sigmav*sqrt(2*pi)))*exp(-(voro(4,ind)-val2)**2/(2*sigmav**2))
                        
                        if ((abs(mean1-val1) > theta1) .or. (abs(mean2-val2)>theta2)) write(*,*)2,val1,val2,&
Voro_prop(3,ind),Voro_prop(4,ind),prob*theta1*theta2*4
                
                else
                        call delaun (voro_prop(1:2,:),ncell_prop,neighbour,vertices,nt,nt_max,&
                                worki1,worki2,worki3,eps,nv_max,&
                                0,ldummy,0,0,0)

                        call build_nv(ncell_prop,vertices,nt,ncell_max,nmax,&
                                neighbour,nnn,nnlist,ntwork)

                        !where is the voro(1:2,ind) in the new tesselation
                        node=1
                        call find_node2D([Voro(1,ind),Voro(2,ind)]&
                        ,node,voro_prop(1:2,:),nnn,nnlist,walk)

                        central_previous=find_central(Voro(1,ind),Voro(2,ind))
                        central=find_central(Voro_prop(1,node),Voro_prop(2,node))

                        prob=(1/(sigmav*sqrt(2*pi)))*exp(-(Voro(3,ind)-Voro_prop(3,node)+central(1)-central_previous(1))**2/&
(2*sigmav**2))*(1/(sigmav*sqrt(2*pi)))*exp(-(Voro(4,ind)-Voro_prop(4,node)-central(2)+central_previous(2))**2/(2*sigmav**2))
                end if
        end if !a==1

   else if (move==1) then
        !    write(*,*)'move'
        ind = 4
        do while(ind==4)
                ind = ceiling(ran3(ra)*(ncell-4))+4
        enddo

        p1 = Voro(1,ind)+(GASDEV(ra)*pd*(longmax-longmin)/100)
        p2 = Voro(2,ind)+(GASDEV(ra)*pd*(latmax-latmin)/100)

        IF ((p1.GT.longmin).and.(p2.GT.latmin).and.&
        (p1.LT.longmax).and.(p2.LT.latmax)) THEN
                if (recursiveprior==1) then
                        central_previous=find_central(Voro(1,ind),Voro(2,ind))
                        central=find_central(p1,p2)
                        a=1
                        Voro_prop(1,ind) = p1
                        Voro_prop(2,ind) = p2
                        Voro_prop(3,ind) = Voro(3,ind)-central_previous(1)+central(1)
                        Voro_prop(4,ind) = Voro(4,ind)-central_previous(2)+central(2)
                else       
                        a=1
                        !write(*,*)'gagne'
                        Voro_prop(1,ind) = p1
                        Voro_prop(2,ind) = p2
                end if
        END IF
        
   end if


! Get like_prop
   if (a==1) then !compute the likelihood.
!
        call fmodel(voro_prop,ncell_prop,location,npoint,dest1,dest2,npointm,ncell_max,nt_max,nmax,nv_max,eps)
        like_prop=0
        ! COMMENTED FOR THE SAFE OF TESTING
        !
         DO i=1,npoint
                ! write(*,*)location(i,:),dobs(i,1),dest(i)
                if (norm==2)then
                        like_prop = like_prop + (((dobs1(i,1)-dest1(i))*dobs1(i,2))**2)/(2*(sigma_prop**2)) +&
 (((dobs2(i,1)-dest2(i))*dobs2(i,2))**2)/(2*(sigma_prop**2)) 
                        !write(*,*)i,like,dobs(i,1),dest(i),dobs(i,2),sigma
                else if (norm==1) then
                        like_prop = like_prop + (abs((dobs1(i,1)-dest1(i))*dobs1(i,2)))/(sigma_prop) +&
 (abs((dobs2(i,1)-dest2(i))*dobs2(i,2)))/(sigma_prop)

                !write(*,*)i,dobs(i,1),dest(i)!,dobs(i,2),sigma
                else if (norm==3) then
                        like_prop = like_prop + log(pi*sigma_prop/dobs1(i,2)) + log(1+((dobs1(i,1)-&
dest1(i))*dobs1(i,2)/sigma_prop)**2) + log(pi*sigma_prop/dobs2(i,2)) + log(1+((dobs2(i,1)-dest2(i))*dobs2(i,2)/sigma_prop)**2) 
                endif
        END DO
   else !if (a==0)
        like_prop = like 
        !write(*,*)'cell outside'
   endif
ELSE  !Move a value %--------------------------------------------------------
        !a=1
        birth=0
        death=0
        ind = ceiling(ran3(ra)*ncell)
        if (ind==0) ind=1
        Voro_prop(3,ind) = Voro(3,ind)+GASDEV(ra)*pv !  
        Voro_prop(4,ind) = Voro(4,ind)+GASDEV(ra)*pv ! 
        if (recursiveprior==0) then
                if (abs(mean1-Voro_prop(3,ind))<theta1 .and. abs(mean2-Voro_prop(4,ind))<theta2) a=1 ! 
        else
                central=find_central(Voro_prop(1,ind),Voro_prop(2,ind))
                if (abs(central(1)-Voro_prop(3,ind))<theta1 .and. abs(central(2)-Voro_prop(4,ind))<theta2) a=1 ! 
        end if
        !The cell geometry does not change
        ! Get like_prop
        if (a==1) then
                like_prop=0
                ! COMMENTED FOR THE SAKE OF TESTING
                call fmodel(voro_prop,ncell_prop,location,npoint,dest1,dest2,npointm,ncell_max,nt_max,nmax,nv_max,eps)
                !
                DO i=1,npoint
                        ! write(*,*)location(i,:),dobs(i,1),dest(i)
                        if (norm==2)then
                                like_prop = like_prop + (((dobs1(i,1)-dest1(i))*dobs1(i,2))**2)/(2*(sigma_prop**2)) +&
 (((dobs2(i,1)-dest2(i))*dobs2(i,2))**2)/(2*(sigma_prop**2)) 
                                !write(*,*)i,like,dobs(i,1),dest(i),dobs(i,2),sigma
                        else if (norm==1) then
                                like_prop = like_prop + (abs((dobs1(i,1)-dest1(i))*dobs1(i,2)))/(sigma_prop) +&
 (abs((dobs2(i,1)-dest2(i))*dobs2(i,2)))/(sigma_prop)

                        !write(*,*)i,dobs(i,1),dest(i)!,dobs(i,2),sigma
                        else if (norm==3) then
                                like_prop = like_prop + log(pi*sigma_prop/dobs1(i,2)) + log(1+((dobs1(i,1)-&
dest1(i))*dobs1(i,2)/sigma_prop)**2) + log(pi*sigma_prop/dobs2(i,2)) + log(1+((dobs2(i,1)-dest2(i))*dobs2(i,2)/sigma_prop)**2) 
                        endif
                END DO
        else !if a==0
                like_prop = like 
        end if        
END IF ! velocity or move

!now see if we accept the proposed change to the model using ratio of posteriors
!!post = like * prior(Voro(:,3),theta,mean,ncell)
!! where like = exp(-1/2 * misfit)
a=a*as 
lgprior_prop=log(real(a))

!lgprior_prop=logprior(Voro_prop(3,:),theta,mean,ncell_prop,a)
        !!!lgprior=logprior(Voro(3,:),theta,mean,ncell)
!write(*,*)prob

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if ((norm==1).or.(norm==2)) then
        lgsigma = (2*npoint)*log(real(sigma/sigma_prop))
        !to be unCOMMENTED FOR THE SAKE OF TESTING
        !lgsigma = 0
elseif (norm==3) then
        lgsigma = 0
endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


 accept=.false.

! to be unCOMMENTED FOR THE SAKE OF TESTING
!like=0
!like_prop=0

  IF (birth==1)THEN
!   remplace 2 theta par 4 theta 
        IF (ran3(ra).LT.((1/(4*(theta1*theta2)*prob))*exp(lgsigma+lgprior_prop-like_prop+like))) accept=.true.
        !write(*,*)1/(4*theta1*theta2*prob),prob,1/(4*theta1*theta2)
        !IF (ran3(ra).LT.((1/(2*(theta1+theta2)*prob))*exp(lgprior_prop))) accept=.true.
        
!  If (ran3(bb).LT.exp((lgprior_prop-like_prop/2)-(lgprior-like/2+log(real(ncell_prop))))) accept=.true.
  ELSE IF (death==1)THEN
        !  THOMAS
        IF (ran3(ra).LT.((4*(theta1*theta2)*prob)*exp(lgsigma+lgprior_prop-like_prop+like))) accept=.true.
        !write(*,*)4*theta1*theta2*prob,prob,4*theta1*theta2
        !IF (ran3(ra).LT.((2*(theta1+theta2)*prob)*exp(lgprior_prop))) accept=.true.



        !  If (ran3(bb).LT.exp((lgprior_prop-like_prop/2+log(real(ncell)))-(lgprior-like/2))) accept=.true.
  ELSE
        IF (ran3(ra).LT.exp(lgsigma+lgprior_prop-like_prop+like)) THEN
!        IF (ran3(ra).LT.exp(lgprior_prop)) THEN
        

 accept=.true.
        if (mod(ount,2).EQ.0) AR1p(1)=AR1p(1)+1
        if (mod(ount,2).EQ.1) AR1v(1)=AR1v(1)+1

        ! IF (ran3(bb).LT.exp((lgprior_prop-like_prop/2)-(lgprior-like/2))) accept=.true.
        ELSE IF (as==0) THEN
                if (mod(ount,2).EQ.0) AR1p(2)=AR1p(2)+1
                if (mod(ount,2).EQ.1) AR1v(2)=AR1v(2)+1
        ELSE IF ((mod(ount,2).EQ.1).and.(DRv.eqv..true.).and.(as==1)) THEN ! if we have rejected a velocity value change 
                !write(*,*)'reject'
                AR1v(2)=AR1v(2)+1
!***********************************************************************************
!*****************START DELAYED REJECTION FOR THE VALUE ***************************
                a2=0
                Voro_pprop=voro
                Voro_pprop(3,ind) = Voro(3,ind)+GASDEV(ra)*pv2
                Voro_pprop(4,ind) = Voro(4,ind)+GASDEV(ra)*pv2
                if (recursiveprior==0) then
                        if (abs(mean1-Voro_pprop(3,ind))<theta1 .and. abs(mean2-Voro_pprop(4,ind))<theta2) a2=1 !
                else
                        central=find_central(Voro_pprop(1,ind),Voro_pprop(2,ind))
                        if (abs(central(1)-Voro_pprop(3,ind))<theta1 .and. abs(central(2)-Voro_pprop(4,ind))<theta2) a2=1 !
                end if
                if (a2==1) then
                        ! Compute the likelihood
                        ! COMMENTED FOR THE SAKE OF TESTING
                        like_pprop=0
                        call fmodel(voro_pprop,ncell_prop,location,npoint,dest1,dest2,npointm,ncell_max,nt_max,nmax,nv_max,eps)
                        DO i=1,npoint
                                ! write(*,*)location(i,:),dobs(i,1),dest(i)
                                if (norm==2)then
                                        like_pprop = like_pprop + (((dobs1(i,1)-dest1(i))*dobs1(i,2))**2)/(2*(sigma_prop**2)) +&
 (((dobs2(i,1)-dest2(i))*dobs2(i,2))**2)/(2*(sigma_prop**2)) 
                                        !write(*,*)i,like,dobs(i,1),dest(i),dobs(i,2),sigma
                                else if (norm==1) then
                                        like_pprop = like_pprop + (abs((dobs1(i,1)-dest1(i))*dobs1(i,2)))/(sigma_prop) +&
 (abs((dobs2(i,1)-dest2(i))*dobs2(i,2)))/(sigma_prop)

                                !write(*,*)i,dobs(i,1),dest(i)!,dobs(i,2),sigma
                                else if (norm==3) then
                                        like_pprop = like_pprop + log(pi*sigma_prop/dobs1(i,2)) + log(1+((dobs1(i,1)-&
dest1(i))*dobs1(i,2)/sigma_prop)**2) + log(pi*sigma_prop/dobs2(i,2)) + log(1+((dobs2(i,1)-dest2(i))*dobs2(i,2)/sigma_prop)**2) 
                                endif
                        END DO
                else !if a2==0
                        like_pprop = like_prop 
                end if
                !to be unCOMMENTED FOR THE SAKE OF TESTING
                !like_pprop = 0
        
                lgprior_pprop=log(real(a2))
                !NOW NEED TO COMPILE THE ALPHA 2
                TT1=exp(lgsigma+(lgprior_pprop-like_pprop)-(-like))
                TT2=exp((-(Voro_pprop(3,ind)-Voro_prop(3,ind))**2+(Voro_prop(3,ind)-Voro(3,ind))**2)/(2*pv**2)+&
(-(Voro_pprop(4,ind)-Voro_prop(4,ind))**2+(Voro_prop(4,ind)-Voro(4,ind))**2)/(2*pv**2)) !

                if (((exp((lgprior_prop-like_prop)-(lgprior_pprop-like_pprop)))>1).or.(a2==0)) then
                        TT3=0
                        
                else
                        TT3= (1-exp((lgprior_prop-like_prop)-(lgprior_pprop-like_pprop)))/(1-exp(lgsigma+&
(lgprior_prop-like_prop)-(-like)))
                end if
                
                !Now compare
                IF (ran3(ra).LT.(TT1*TT2*TT3)) THEN
                        accept=.true.
                        AR2v(1)=AR2v(1)+1
                        like_prop = like_pprop
                        Voro_prop = Voro_pprop
                ELSE 
                        AR2v(2)=AR2v(2)+1
                END IF
                !if (a==0) write(*,*)TT1,TT2,TT3,accept
                
!*****************END DELAYED REJECTION FOR THE VALUE ***************************
!********************************************************************************
        ELSE IF ((mod(ount,2).EQ.0).and.(DRp.eqv..true.).and.(as==1)) THEN ! if we have rejected a nucleus move 
                !write(*,*)'reject'
                AR1p(2)=AR1p(2)+1

!***********************************************************************************
!*****************START DELAYED REJECTION FOR THE POSITION ***************************
                
                a2=0
                Voro_pprop=voro
                
                p1 = Voro(1,ind)+(GASDEV(ra)*pd2*(longmax-longmin)/100)
                p2 = Voro(2,ind)+(GASDEV(ra)*pd2*(latmax-latmin)/100)
                        
                IF ((p1.GT.longmin).and.(p2.GT.latmin).and.&
                (p1.LT.longmax).and.(p2.LT.latmax)) THEN
                        if (recursiveprior==1) then
                                central_previous=find_central(Voro(1,ind),Voro(2,ind))
                                central=find_central(p1,p2)
                                a2=1
                                Voro_pprop(1,ind) = p1
                                Voro_pprop(2,ind) = p2
                                Voro_pprop(3,ind) = Voro(3,ind)-central_previous(1)+central(1)
                                Voro_pprop(4,ind) = Voro(4,ind)-central_previous(2)+central(2)
                        else

                                a2=1
                                !write(*,*)'gagne'
                                Voro_pprop(1,ind) = p1
                                Voro_pprop(2,ind) = p2
                        end if
                END IF
                
                !Get like_prop for the second try
                if (a2==1) then !compute the likelihood.
                ! COMMENTED FOR THE SAKE OF TESTING
                        like_pprop=0
                        call fmodel(voro_pprop,ncell_prop,location,npoint,dest1,dest2,npointm,ncell_max,nt_max,nmax,nv_max,eps)
                        DO i=1,npoint
                                ! write(*,*)location(i,:),dobs(i,1),dest(i)
                                if (norm==2)then
                                        like_pprop = like_pprop + (((dobs1(i,1)-dest1(i))*dobs1(i,2))**2)/(2*(sigma_prop**2)) +&
 (((dobs2(i,1)-dest2(i))*dobs2(i,2))**2)/(2*(sigma_prop**2)) 
                                        !write(*,*)i,like,dobs(i,1),dest(i),dobs(i,2),sigma
                                else if (norm==1) then
                                        like_pprop = like_pprop + (abs((dobs1(i,1)-dest1(i))*dobs1(i,2)))/(sigma_prop) +&
 (abs((dobs2(i,1)-dest2(i))*dobs2(i,2)))/(sigma_prop)

                                !write(*,*)i,dobs(i,1),dest(i)!,dobs(i,2),sigma
                                else if (norm==3) then
                                        like_pprop = like_pprop + log(pi*sigma_prop/dobs1(i,2)) + log(1+((dobs1(i,1)-dest1(i))&
*dobs1(i,2)/sigma_prop)**2) + log(pi*sigma_prop/dobs2(i,2)) + log(1+((dobs2(i,1)-dest2(i))*dobs2(i,2)/sigma_prop)**2) 
                                endif
                        END DO
                else !if (a2==0)
                        like_pprop = like_prop 
                ! write(*,*)'cell outside'
                endif
                
                !to be unCOMMENTED FOR THE SAKE OF TESTING
                !like_pprop = 0

                lgprior_pprop=log(real(a2))
                !NOW NEED TO COMPILE THE ALPHA 2
                TT1=exp(lgsigma+(lgprior_pprop-like_pprop)-(-like))
                TT2a=exp((-(Voro_pprop(1,ind)-Voro_prop(1,ind))**2+(Voro_prop(1,ind)-Voro(1,ind))**2)/(2*pd**2))
                TT2b=exp((-(Voro_pprop(2,ind)-Voro_prop(2,ind))**2+(Voro_prop(2,ind)-Voro(2,ind))**2)/(2*pd**2))
                
                if (((exp((lgprior_prop-like_prop)-(lgprior_pprop-like_pprop)))>1).or.(a2==0)) then
                        TT3=0
                else
                        TT3= (1-exp((lgprior_prop-like_prop)-(lgprior_pprop-like_pprop)))/&
(1-exp(lgsigma+(lgprior_prop-like_prop)-(-like)))
                end if
                        
                !Now compare
                IF (ran3(ra).LT.(TT1*TT2a*TT2b*TT3)) THEN
                        accept=.true.
                        !accept a second time
                        !if (mod(ount,2).NE.0) 
                        AR2p(1)=AR2p(1)+1
                        !if (mod(ount,2).EQ.0) AR2p(1)=AR2p(1)+1
                        like_prop = like_pprop
                        Voro_prop = Voro_pprop
                ELSE 
                        AR2p(2)=AR2p(2)+1
                END IF        


!*****************END DELAYED REJECTION FOR THE POSITION ***************************
!********************************************************************************
        END IF
END IF


IF (accept.eqv..true.) THEN
        ! we accept the proposed changes: hence update the  state of the chain
        AR(1)=AR(1)+1
        like = like_prop
        Voro = Voro_prop
        sigma = sigma_prop
        IF (mod(ount,2).EQ.0) THEN
                ncell = ncell_prop
                If (birth==1) ARB(1)=ARB(1)+1 
                If (birth==1) b=b+1
                If (death==1) ARD(1)=ARD(1)+1 
                If (death==1) d=d+1 
                If (move==1)  ARM(1)=ARM(1)+1
        ELSE
                ARV(1)=ARV(1)+1
        END IF
ELSE
        AR(2)=AR(2)+1
        IF (mod(ount,2).EQ.0) THEN
                If (birth==1) ARB(2)=ARB(2)+1 
                If (move==1)  ARM(2)=ARM(2)+1
                If (death==1)  ARD(2)=ARD(2)+1
        ELSE
                ARV(2)=ARV(2)+1
        END IF
END IF

AR(3)=100*AR(1)/(AR(2)+AR(1))
ARB(3)=100*ARB(1)/(ARB(2)+ARB(1))
ARD(3)=100*ARD(1)/(ARD(2)+ARD(1))
ARM(3)=100*ARM(1)/(ARM(2)+ARM(1))
ARV(3)=100*ARV(1)/(ARV(2)+ARV(1))
AR1p(3)=100*AR1p(1)/(AR1p(2)+AR1p(1))
AR2p(3)=100*AR2p(1)/(AR2p(2)+AR2p(1))
AR1v(3)=100*AR1v(1)/(AR1v(2)+AR1v(1))
AR2v(3)=100*AR2v(1)/(AR2v(2)+AR2v(1))
!ncellav=ncellav+ncell


         ! Store models for restart
         IF ((mod(ount,store)==0)) THEN 
              write(number,1000)rank
              filename=number//'model.dat'
              open(rank*10,file='Store/'//filename,status='unknown') 
                      write(rank*10,*)nbproc
                      write(rank*10,*)ncell
                      do i=1,ncell
                              write(rank*10,*)voro(:,i)
                      enddo
              close(rank*10)
         ENDIF


!* STORE THE SAMPLES


!Get the Mean 'AVE' updated and the Variance. Variance is computed from 
!the formula var^2 = <(x-<x>)^2> = <x^2> - <x>^2 
IF (ount.GT.burn_in) THEN !start collecting samples
        sample = sample+1
        IF (mod(ount,thinn)==0) THEN
                thin = thin + 1
                ncellav = ncellav+ncell
                sigav = sigav + sigma
                Evidence(ncell)=Evidence(ncell)+1
                call delaun (voro(1:2,:),ncell,neighbour,vertices,nt,nt_max,&
                                worki1,worki2,worki3,eps,nv_max,&
                                0,ldummy,0,0,0)
                call build_nv(ncell,vertices,nt,ncell_max,nmax,&
                        neighbour,nnn,nnlist,ntwork)
                loc = 1
                DO i=1,nvp
                        DO j=1,nvt
                                p1=longmind+(i-1)*long
                                p2=latmind+(j-1)*lat
                                call Triloc_del(p1,p2,Voro(1:2,:),vertices,neighbour,loc,eps,boolval,k,iface)
                                vert=vertices(1:3,loc)
                                pos1=Voro(1,vert(1:3))
                                pos2=Voro(2,vert(1:3))
                                pos3=Voro(3,vert(1:3))
                                pos4=Voro(4,vert(1:3))
                                SM=(/ -1,-1,-1 /)
                                AA1(1:3,1)=pos1
                                AA1(1:3,2)=pos2
                                AA1(1:3,3)=pos3
                                X1=solve3(AA1,SM)
                                val1=(-p1*X1(1)-p2*X1(2)-1)/X1(3)
                                dVxdx=-X1(1)/(X1(3)*1000000)
                                dVxdy=-X1(2)/(X1(3)*1000000)

                                !dVxdx=-X1(1)/X1(3)
                                !dVxdy=-X1(2)/X1(3)
                                
                                AA2(1:3,1)=pos1
                                AA2(1:3,2)=pos2
                                AA2(1:3,3)=pos4
                                X2=solve3(AA2,SM)
                                val2=(-p1*X2(1)-p2*X2(2)-1)/X2(3)
                                !if (abs(val2) .GT. 1000) write(*,*)p1,p2,X2(1),X2(2),X2(3)
                                dVydx=-X2(1)/(X2(3)*1000000)
                                dVydy=-X2(2)/(X2(3)*1000000)
                               
 
                                phi=ATAN((dVxdy+dVydx)/(dVxdx-dVydy))/2
                                RT=reshape( (/ COS(phi), SIN(phi), &
                                              -SIN(phi), COS(phi) /), &
                                           shape(RT), order=(/2,1/) )

                                if(lagrangian==1) then
                                        !Lagrangian definition of the strain rate tensor E
                                        E=reshape( (/ dVxdx+(dVxdx*dVxdx)/2, (dVxdy+dVydx+dVxdy*dVydx)/2, &
                                                    (dVxdy+dVydx+dVxdy*dVydx)/2, dVydy+(dVydy*dVydy)/2 /), &
                                                  shape(E), order=(/2,1/) )
                                else
                                        !Infinitesimal definition of the strain rate tensor E
                                        E=reshape( (/ dVxdx, (dVxdy+dVydx)/2, &
                                                    (dVxdy+dVydx)/2, dVydy /), &
                                                  shape(E), order=(/2,1/) )
                                end if

                                lambda=matmul(RT,matmul(E,transpose(RT))) 

                                if (IsNaN(val1) .or. IsNaN(val2)) then
                                        write(*,*)X1(3),X2(3)
                                end if
                                        
                                jj7=ceiling((phi+pi/4)*9/(pi/2))
                                LAMBDA1(i,j,jj7) = LAMBDA1(i,j,jj7) + lambda(1,1)
                                LAMBDA2(i,j,jj7) = LAMBDA2(i,j,jj7) + lambda(2,2)
                                postphi(i,j,jj7) = postphi(i,j,jj7)+1


                                AVE1(i,j) = AVE1(i,j) + val1!     
                                AVE2(i,j) = AVE2(i,j) + val2!
                                VAR1(i,j) = VAR1(i,j) + val1**2 !
                                VAR2(i,j) = VAR2(i,j) + val2**2 !
                                !AVEDIV(i,j) = AVEDIV(i,j) + (dVxdx+dVydy)
                                AVEDIV(i,j) = AVEDIV(i,j) + (E(1,1)+E(2,2)) !generalisation
                                AVEROT(i,j) = AVEROT(i,j) + (dVydx-dVxdy)
                                !INV2(i,j) = INV2(i,j) + sqrt(dVxdx**2+dVydy**2+((dVxdy+dVydx)**2)/2) !american 2nd inv
                                INV2(i,j) = INV2(i,j) + sqrt(E(1,1)**2+E(2,2)**2+((E(1,2)+E(2,1))**2)/2)!generalisation
                                !INV2(i,j) = INV2(i,j) + dVxdx*dVydy-((dVxdy+dVydx)**2)/4 !french 2nd inv
 
                                !warning nonzero is too sensitive E-15 is considered nonzero. can be commented
                                if (abs(lambda(1,2)).GT.nonzero .or. abs(lambda(2,1)).GT.nonzero) then
                                        write(*,*)'Diagonalisation failed',lambda(1,2),lambda(2,1)
                                end if
                                !AVEMOM(i,j) = AVEMOM(i,j) + (dVydy+dVxdx+sqrt((dVxdx-dVydy)**2+(dVxdy+dVydx)**2))/2


                                ! THOMAS
                                if (recursiveprior==0) then
                                        jj1=ceiling((val1-(mean1-theta1))*nvd/(2*theta1))
                                        jj2=ceiling((val2-(mean2-theta2))*nvd/(2*theta2))
                                else
                                        jj1=ceiling((val1-(mean1-theta1old))*nvd/(2*theta1old))
                                        jj2=ceiling((val2-(mean2-theta2old))*nvd/(2*theta2old))
                                end if
                                jj3=ceiling(((dVxdx+dVydy)-divmin)*nvd/(divmax-divmin))
                                jj4=ceiling(((dVydx-dVxdy)-rotmin)*nvd/(rotmax-rotmin))
                                jj5=ceiling((sqrt(dVxdx**2+dVydy**2+((dVxdy+dVydx)**2)/2)-invmin)*nvd/(invmax-invmin)) !american 2nd inv
                                !jj5=ceiling(((dVxdx*dVydy-((dVxdy+dVydx)**2)/4)-invmin)*nvd/(invmax-invmin)) !french 2nd inv
                                !jj6=ceiling(((dVydy+dVxdx+sqrt((dVxdx-dVydy)**2+(dVxdy+dVydx)**2)/2)-mommin)*nvd/(mommax-mommin))

!ATTENTION : CETTE DEFINITION DE AXIS1 ET AXIS2 DONNE AXIS1 = Y' et AXIS2 = X'                                
                                jj10=ceiling(((val1*COS(axisrot)+val2*SIN(axisrot))-axisrotmin1)*nvd/(axisrotmax1-axisrotmin1))
                                jj11=ceiling(((val2*COS(axisrot)-val1*SIN(axisrot))-axisrotmin2)*nvd/(axisrotmax2-axisrotmin2))

                                if (jj1.GT.nvd .or. jj2.GT.nvd .or. jj1.LT.0 .or. jj2.LT.0) then
                                        write(*,*)p1,p2,jj1,jj2,val1,val2,pos1,pos2,pos3,pos4,vert,AA1
                                end if
                                post1(i,j,jj1)=post1(i,j,jj1)+1
                                post2(i,j,jj2)=post2(i,j,jj2)+1
                                if (jj3.GT.nvd) jj3=nvd
                                if (jj4.GT.nvd) jj4=nvd
                                if (jj5.GT.nvd) jj5=nvd
                                if (jj6.GT.nvd) jj6=nvd
                                if (jj7.GT.nvd) jj7=nvd
                                if (jj10.GT.nvd) jj10=nvd
                                if (jj11.GT.nvd) jj11=nvd
                                if (jj3.LT.1) jj3=1
                                if (jj4.LT.1) jj4=1
                                if (jj5.LT.1) jj5=1
                                if (jj6.LT.1) jj6=1
                                if (jj7.LT.1) jj7=1
                                if (jj10.LT.1) jj10=1
                                if (jj11.LT.1) jj11=1
                                postdiv(i,j,jj3)=postdiv(i,j,jj3)+1
                                postrot(i,j,jj4)=postrot(i,j,jj4)+1
                                postinv(i,j,jj5)=postinv(i,j,jj5)+1
                                !POSTMOM(i,j,jj6)=postmom(i,j,jj6)+1 
                                post1axis(i,j,jj10)=post1axis(i,j,jj10)+1
                                post2axis(i,j,jj11)=post2axis(i,j,jj11)+1
                                
                        END DO
                END DO
                ! CONSTUCT THE DENSITY MAP
                DO node=1,ncell
                                ii=ceiling((Voro(1,node)+(0.5*long)-longmind)/real(long))
                                jj=ceiling((Voro(2,node)+(0.5*lat)-latmind)/real(lat))
                                if ((ii>0).and.(jj>0).and.(ii<(nvp+1)).and.(jj<(nvt+1))) then
                                        density(ii,jj)=density(ii,jj)+1
                                endif
                ENDDO

        ii=ceiling((sigma-sigma_min)*180/(sigma_max-sigma_min))
        ML_sigma(ii) = ML_sigma(ii)+1
        END IF
END IF
!******************************************
!WINDOW AVERAGE
!******************************************
IF (ount.GT.win) THEN
        Do i=1,win-1
                WA(i)=WA(i+1)
        End do
        WA(win)=ncell
        !write(*,*)
        !write(*,*)ncell
        !if (rank==0) write(*,*)WA
        winav=0
        winva=0
        Do i=1,win
        winav=winav+WA(i)
        winva=winva+WA(i)**2
        End Do
        winav=winav/real(win)
        winva=winva/real(win)
        !if (rank==0) write(*,*)
        !if (rank==0) write(*,*)winav
        AVW(ount-win,1)=winav
        AVW(ount-win,2)=winva
        !if (rank==0) write(*,*)
        !if (rank==0) write(*,*)AVW
        !if (rank==0) stop
ELSE
        w=w+1
        WA(w)=ncell
        !write(*,*)w,ncell
END IF

!**********************************************************************        
! Display what is going on
IF (mod(ount,display).EQ.0) THEN
        write(*,*)'proc',rank
        write(*,*)'sigma',sigma,'sigma_av',sigav/thin
        write(*,*)'sample:',ount,'/',sampletotal,', ncell:',ncell,&
        ', misfit:',like*(2*(sigma**2))
        write(*,*)'AR1v',AR1v(3),'AR2v',AR2v(3),'ARV:',ARV(3)
        write(*,*)'AR1p',AR1p(3),'AR2p',AR2p(3),'ARM:',ARM(3)
        write(*,*)'AR:',AR(3),'ARB:',ARB(3),'ARD:',ARD(3)
        write(*,*)'ncellav',ncellav/thin,'---------------------------------'
        write(*,*)
        write(*,*)'---------------------'

!do i=1,npoint
  ! write(*,*)i,location(i,1),location(i,2),dobs(i,1),dest(i)
!enddo
!write(*,*)'---------------------'

        
END IF

END DO !end the sampling

!if (rank==0) close(51)


AVE1 = AVE1/thin
VAR1 = VAR1/thin
AVE2 = AVE2/thin
VAR2 = VAR2/thin
AVEDIV = AVEDIV/thin
AVEROT = AVEROT/thin
INV2 = INV2/thin
!AVEMOM = AVEMOM/thin
density = density/thin



!VAR = sqrt(VAR/thin - AVE**2)
!*******************************************
!Colect all the AVE and VAR from all the chains
call MPI_REDUCE(ML_sigma,ML_sigmas,180,MPI_integer,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(AVW,AVWS,2*(nsample+burn_in-win),MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(AVE1,AVES1,nvp*nvt,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)!
call MPI_REDUCE(VAR1,VARS1,nvp*nvt,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)!
call MPI_REDUCE(AVE2,AVES2,nvp*nvt,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)!
call MPI_REDUCE(VAR2,VARS2,nvp*nvt,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)!
call MPI_REDUCE(AVEDIV,AVEDIVS,nvp*nvt,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)!
call MPI_REDUCE(AVEROT,AVEROTS,nvp*nvt,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)!
call MPI_REDUCE(INV2,INV2S,nvp*nvt,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(LAMBDA1,LAMBDA1S,nvp*nvt*9,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(LAMBDA2,LAMBDA2S,nvp*nvt*9,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
!call MPI_REDUCE(AVEMOM,AVEMOMS,nvp*nvt,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(density,densitys,nvp*nvt,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(post1,posts1,nvp*nvt*nvd,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(postdiv,postsdiv,nvp*nvt*nvd,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(postrot,postsrot,nvp*nvt*nvd,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(postinv,postsinv,nvp*nvt*nvd,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(postmom,postsmom,nvp*nvt*nvd,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(postphi,postsphi,nvp*nvt*nvd,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(post2,posts2,nvp*nvt*nvd,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(post1axis,posts1axis,nvp*nvt*nvd,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(post2axis,posts2axis,nvp*nvt*nvd,MPI_Real,MPI_SUM,0,MPI_COMM_WORLD,ierror)
call MPI_REDUCE(Evidence,EvidenceS,ncell_max,MPI_integer,MPI_SUM,0,MPI_COMM_WORLD,ierror)



if (rank==0) then
!Different calculation for the average of LAMBDA1 and LAMBDA2 : use postphi
DO i=1,nvp
        DO j=1,nvt
                DO k=1,9
                        if (postsphi(i,j,k)==0) then
                                !write(*,*)'ERROR',i,j,k
                                LAMBDA1S(i,j,k) = 0
                                LAMBDA2S(i,j,k) = 0
                        else       
                                LAMBDA1S(i,j,k) = LAMBDA1S(i,j,k)/postsphi(i,j,k)
                                LAMBDA2S(i,j,k) = LAMBDA2S(i,j,k)/postsphi(i,j,k)
                        end if
                END DO
        END DO
END DO
end if


AVES1=AVES1/nbproc
VARS1=VARS1/nbproc
AVES2=AVES2/nbproc
VARS2=VARS2/nbproc
AVEDIVS=AVEDIVS/nbproc
AVEROTS=AVEROTS/nbproc
INV2S=INV2S/nbproc
!AVEMOMS=AVEMOMS/nbproc
densitys=densitys/nbproc
VARS1 = sqrt(VARS1 - AVES1**2)
VARS2 = sqrt(VARS2 - AVES2**2)
AVWS=AVWS/nbproc
AVWS(:,2)=sqrt(AVWS(:,2)-AVWS(:,1)**2)
!***********************************************************************
! Take the max of posts and median
theta1max=0
theta2max=0
DO i=1,nvp
        DO j=1,nvt
                if (recursiveprior==1) then
                        central(1)=central1_old(i,j)
                        central(2)=central2_old(i,j)
                end if
                maxx1=0
                maxx2=0
                ! Take the max of posts 
                ind=1
                pbis1=0
                pbis2=0
                do jj=1,nvd
                        pbis1=pbis1+posts1(i,j,jj)
                        if (posts1(i,j,jj)>maxx1) then
                                ind=jj
                                maxx1=posts1(i,j,jj)
                        endif
                enddo
                if (recursiveprior==0) then
                        maxmap1(i,j)=mean1-theta1+(ind-0.5)*2*theta1/nvd
                else
                        maxmap1(i,j)=mean1-theta1old+(ind-0.5)*2*theta1old/nvd
                end if
                do jj=1,nvd
                        pbis2=pbis2+posts2(i,j,jj)
                        if (posts2(i,j,jj)>maxx2) then
                                ind=jj
                                maxx2=posts2(i,j,jj)
                        endif
                enddo
                if (recursiveprior==0) then
                        maxmap2(i,j)=mean2-theta2+(ind-0.5)*2*theta2/nvd
                else
                        maxmap2(i,j)=mean2-theta2old+(ind-0.5)*2*theta2old/nvd
                end if
                
                ! Take the median of posts
                pp1=0
                pp2=0
                do jj=1,nvd
                        pp1=pp1+posts1(i,j,jj)
                        if (pp1>(pbis1/2)) then
                                ind=jj
                                if (recursiveprior==0) then
                                        median1(i,j)=mean1-theta1+(ind-0.5)*2*theta1/nvd
                                else
                                        median1(i,j)=mean1-theta1old+(ind-0.5)*2*theta1old/nvd
                                end if
                                exit
                        endif
                enddo
                do jj=1,nvd
                        pp2=pp2+posts2(i,j,jj)
                        if (pp2>(pbis2/2)) then
                                ind=jj 
                                if (recursiveprior==0) then
                                        median2(i,j)=mean2-theta2+(ind-0.5)*2*theta2/nvd
                                else
                                        median2(i,j)=mean2-theta2old+(ind-0.5)*2*theta2old/nvd
                                end if
                                exit
                        endif
                enddo
                
                ! Take the center of distribution after removing top 20% and
                ! bottom 20% for recursive calculation
                pp1=0
                pp2=0
                minbound=-10000
                do jj=1,nvd
                        pp1=pp1+posts1(i,j,jj)
                        if (pp1>(pbis1/20) .and. minbound==-10000) then
                                ind=jj
                                if (recursiveprior==0) then
                                        minbound=mean1-theta1+(ind-0.5)*2*theta1/nvd
                                else
                                        minbound=mean1-theta1old+(ind-0.5)*2*theta1old/nvd
                                end if
                        endif
                        if (pp1>(19*pbis1/20)) then
                                ind=jj
                                if (recursiveprior==0) then
                                        maxbound=mean1-theta1+(ind-0.5)*2*theta1/nvd
                                else
                                        maxbound=mean1-theta1old+(ind-0.5)*2*theta1old/nvd
                                end if
                                exit
                        endif
                enddo
                central1_new(i,j)=(maxbound+minbound)/2
                !if ((maxbound-minbound)/2>theta1max) then
                !        write(*,*)maxbound,minbound,theta1max
                !        theta1max=(maxbound-minbound)/2
                !endif

                minbound=-10000
                do jj=1,nvd
                        pp2=pp2+posts2(i,j,jj)
                        if (pp2>(pbis2/20) .and. minbound==-10000) then
                                ind=jj
                                if (recursiveprior==0) then
                                        minbound=mean2-theta2+(ind-0.5)*2*theta2/nvd
                                else
                                        minbound=mean2-theta2old+(ind-0.5)*2*theta2old/nvd
                                end if
                        endif
                        if (pp2>(19*pbis2/20)) then
                                ind=jj
                                if (recursiveprior==0) then
                                        maxbound=mean2-theta2+(ind-0.5)*2*theta2/nvd
                                else
                                        maxbound=mean2-theta2old+(ind-0.5)*2*theta2old/nvd
                                end if
                                exit
                        endif
                enddo
                central2_new(i,j)=(maxbound+minbound)/2

                if ((maxbound-minbound)/2>theta2max) then
                        theta2max=(maxbound-minbound)/2
                endif
        END DO
END DO


!***********************************************************************
!*Write the results
IF (rank==0) THEN

! open(4,file='posterior.out',status='replace')
! ii=50
! jj=50
! p1=longmin+(ii-1)*long
! p2=latmin+(jj-1)*lat
! !write(4,*)p1,p2
! do i=1,nvd
! p1=mean-theta+(i-0.5)*2*theta/nvd
! p2=posts(ii,jj,i)
! write(4,*)p1,p2
! enddo
! close(4)


 open(411,file='OUT/Residuals1.out',status='replace')
do i=1,npoint
write(411,*)dobs1(i,1),dest1(i),sigma/dobs1(i,2),sigma
end do
 close(411)

 open(412,file='OUT/Residuals2.out',status='replace')
do i=1,npoint
write(412,*)dobs2(i,1),dest2(i),sigma/dobs2(i,2),sigma
end do
 close(412)

open(44,file='OUT/NB_cells.out',status='replace')
 do i=1,nsample+burn_in-win
 write(44,*)AVWS(i,:)
 end do
 close(44)! close the file with Evidence

open(33,file='OUT/conv_sigma.out',status='replace')
 do i=1,nsample+burn_in-win
 write(33,*)conv_sigma(i)
 end do
 close(33) ! close the file with Evidence

        open(54,file='OUT/Evidence.out',status='replace')
        do i=1,ncell_max
                write(54,*)EvidenceS(i)
        end do
        close(54)! close the file with Evidence
                
        open(59,file='OUT/ML_sigma.out',status='replace')
        do i=1,180
        write(59,*)sigma_min+i*(sigma_max-sigma_min)/180,ML_sigmas(i)
        end do
        close(59)! close the file with Evidence

        !*Write the results
        open(520,file='OUT/Average.out',status='unknown')

    open(21,file='OUT/Density.out',status='unknown')
        WRITE(21,*)nvt,nvp
        WRITE(21,'(3f14.8)')latmaxd,longmind
        WRITE(21,'(3f14.8)')lat,long
        WRITE(21,'(1X)')

        open(22,file='OUT/param.out',status='unknown')
        WRITE(22,*)burn_in,nsample
        WRITE(22,*)nvt,nvp,nvd
        WRITE(22,'(4f14.4)')longmind,longmaxd,latmind,latmaxd
        WRITE(22,'(4f14.4)')mean1,theta1,mean2,theta2
        WRITE(22,'(3f14.10)')divmin,divmax
        WRITE(22,'(3f14.10)')rotmin,rotmax
        WRITE(22,'(3f14.10)')invmin,invmax
        WRITE(22,'(1X)')
        close(22)

        open(531,file='OUT/Standard_deviation1.out',status='unknown')
        WRITE(531,*)nvt,nvp
        WRITE(531,'(3f14.8)')latmaxd,longmind
        WRITE(531,'(3f14.8)')lat,long
        WRITE(531,'(1X)')

        open(532,file='OUT/Standard_deviation2.out',status='unknown')
        WRITE(532,*)nvt,nvp
        WRITE(532,'(3f14.8)')latmaxd,longmind
        WRITE(532,'(3f14.8)')lat,long
        WRITE(532,'(1X)')
        
        open(541,file='OUT/Median1.out',status='unknown')
        WRITE(541,*)nvt,nvp
        WRITE(541,'(3f14.8)')latmaxd,longmind
        WRITE(541,'(3f14.8)')lat,long
        WRITE(541,'(1X)')

        open(542,file='OUT/Median2.out',status='unknown')
        WRITE(542,*)nvt,nvp
        WRITE(542,'(3f14.8)')latmaxd,longmind
        WRITE(542,'(3f14.8)')lat,long
        WRITE(542,'(1X)')

        open(543,file='OUT/central.out',status='unknown')
        WRITE(543,*)theta1max,theta2max

        open(501,file='OUT/Maximum1.out',status='unknown')
        WRITE(501,*)nvt,nvp
        WRITE(501,'(3f14.8)')latmaxd,longmind
        WRITE(501,'(3f14.8)')lat,long
        WRITE(501,'(1X)')

        open(502,file='OUT/Maximum2.out',status='unknown')
        WRITE(502,*)nvt,nvp
        WRITE(502,'(3f14.8)')latmaxd,longmind
        WRITE(502,'(3f14.8)')lat,long
        WRITE(502,'(1X)')
        
        open(551,file='OUT/Posterior1.out',status='unknown')
        
        open(552,file='OUT/Posterior2.out',status='unknown')
        
        open(553,file='OUT/Posteriordiv.out',status='unknown')
                
        open(554,file='OUT/Posteriorrot.out',status='unknown')
        
        open(555,file='OUT/Posteriorinv.out',status='unknown')
        
        open(556,file='OUT/Posteriormom.out',status='unknown')

        open(557,file='OUT/Averagelambda1.out',status='unknown')

        open(558,file='OUT/Averagelambda2.out',status='unknown')

        open(559,file='OUT/Posteriorphi.out',status='unknown')

        open(560,file='OUT/Posterior1axis.out',status='unknown')

        open(561,file='OUT/Posterior2axis.out',status='unknown')


DO i=0,nvp+1
                DO j=0,nvt+1
                        ii=i
                        jj=j
                        if (i==0)   ii=1
                        if (j==0)   jj=1
                        if (i==nvp+1) ii=nvp
                        if (j==nvt+1) jj=nvt
                                                
                        write(520,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,AVES1(ii,jj),&
AVES2(ii,jj),AVEDIVS(ii,jj),AVEROTS(ii,jj),INV2S(ii,jj)!,AVEMOMS(ii,jj)!
                        write(531,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,VARS1(ii,jj)!
                        write(532,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,VARS2(ii,jj)!
                        write(501,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,maxmap1(ii,jj)
                        write(502,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,maxmap2(ii,jj)
                        write(541,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,median1(ii,jj)
                        write(542,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,median2(ii,jj)
                        write(543,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,central1_new(ii,jj),central2_new(ii,jj)
                        write(21,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,densitys(ii,jj)
                        if ((i.NE.0).and.(i.NE.nvp+1).and.(j.NE.0).and.(j.NE.nvt+1)) then
                                do p=1,nvd
                                        write(551,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,posts1(ii,jj,p) !
                                        write(552,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,posts2(ii,jj,p) !
                                        write(553,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,postsdiv(ii,jj,p) !
                                        write(554,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,postsrot(ii,jj,p) !
                                        write(555,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,postsinv(ii,jj,p) !
                                        write(556,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,postsmom(ii,jj,p) !
                                        write(560,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,posts1axis(ii,jj,p) !
                                        write(561,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,posts2axis(ii,jj,p) !
                                        
                                enddo
                                do p=1,9
                                        write(557,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,LAMBDA1S(ii,jj,p) !
                                        write(558,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,LAMBDA2S(ii,jj,p) !
                                        write(559,*)longmind+(ii-1)*long,latmind+(jj-1)*lat,postsphi(ii,jj,p) !
                                enddo

                        endif
                        
                        
                        
                END DO
        END DO
        close(531)! close the file with Average
        close(501)! close the file with Average
        close(541)
        close(551)
        close(21)
        close(532)! close the file with Average
        close(502)! close the file with Average
        close(542)
        close(543)
        close(552)
        close(553)
        close(554)
        close(555)
        close(556)
        close(557)
        close(558)
        close(559)
        close(560)
        close(561)
        
END IF




call MPI_FINALIZE(ierror)


CALL cpu_time(t2)
write(*,*)'time taken by the code was',t2-t1,'seconds'


!********************************************
! Function to calculate the central value the distribution in the recursive
! case
!----------------------------------------------------------
CONTAINS
FUNCTION find_central(Xloc,Yloc)

        Double precision Xloc, Yloc
        INTEGER nnvp,nnvt
        REAL find_central(2)

       
        nnvp=floor(((Xloc-longmind)/(longmaxd-longmind))*(nvp-1))+1
        nnvt=floor(((Yloc-latmind)/(latmaxd-latmind))*(nvt-1))+1
        if (nnvt<1) write(*,*)Yloc,birth,death
        find_central(1)=central1_old(nnvp,nnvt)
        find_central(2)=central2_old(nnvp,nnvt)
END

END

!________________________________________________________

FUNCTION GASDEV(idum)

!     ..Arguments..
      integer          idum
      real GASDEV

!     ..Local..
      real v1,v2,r,fac
      real ran3

      if (idum.lt.0) iset=0
  10   v1=2*ran3(idum)-1
       v2=2*ran3(idum)-1
       r=v1**2+v2**2
       if (r.ge.1.or.r.eq.0) GOTO 10
       fac=sqrt(-2*log(r)/r)
       GASDEV=v2*fac

      RETURN
      END
!
!-------------------------------------------------------------------
!                                                
!        Numerical Recipes random number generator (used by NA_random)
!
! ----------------------------------------------------------------------------
      FUNCTION ran3(idum)
      INTEGER idum
      INTEGER MBIG,MSEED,MZ
!     REAL MBIG,MSEED,MZ
      REAL ran3,FAC
      PARAMETER (MBIG=1000000000,MSEED=161803398,MZ=0,FAC=1./MBIG)
!     PARAMETER (MBIG=4000000.,MSEED=1618033.,MZ=0.,FAC=1./MBIG)
      INTEGER i,iff,ii,inext,inextp,k
      INTEGER mj,mk,ma(55)
!     REAL mj,mk,ma(55)
      SAVE iff,inext,inextp,ma
      DATA iff /0/
!      write(*,*)' idum ',idum
      if (idum.lt.0.or.iff.eq.0)then
        iff=1
        mj=MSEED-iabs(idum)
        mj=mod(mj,MBIG)
        ma(55)=mj
        mk=1
        do 11 i=1,54
          ii=mod(21*i,55)
          ma(ii)=mk
          mk=mj-mk
          if (mk.lt.MZ)mk=mk+MBIG
          mj=ma(ii)
11      continue
        do 13 k=1,4
          do 12 i=1,55
            ma(i)=ma(i)-ma(1+mod(i+30,55))
            if (ma(i).lt.MZ)ma(i)=ma(i)+MBIG
12        continue
13      continue
        inext=0
        inextp=31
        idum=1
      endif
      inext=inext+1
      if (inext.eq.56)inext=1
      inextp=inextp+1
      if (inextp.eq.56)inextp=1
      mj=ma(inext)-ma(inextp)
      if (mj.lt.MZ)mj=mj+MBIG
      ma(inext)=mj
      ran3=mj*FAC
      return
      END

!-------------------------------------------------------------------
!Function to find the central value associated to any point within the grid
!------------------------------------------------------------------
